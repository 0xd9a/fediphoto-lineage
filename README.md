# FediPhoto-Lineage

Post a photo quickly to the Fediverse...part II

[https://silkevicious.codeberg.page/fediphoto-lineage.html](https://silkevicious.codeberg.page/fediphoto-lineage.html)

*Open, Click, Shared!*

This is a continuation of the [FediPhoto](https://github.com/pla1/FediPhoto) project by **PLA**, who left us in January 2021. We'll do our best to honour his memory and his work.

## Install

You have many ways to download this app:

<table>

<tr>
<th align="center" valign="center" width="33%">
<h3>Codeberg Releases</h3>
</th>
<th align="center" valign="center" width="33%">
<h3>Silkevicious ApkRepo</h3>
</th>
<th align="center" valign="center" width="33%">
<h3>F-Droid</h3>
</th>
</tr>

<tr>
<td align="center" valign="center">
<p>
<a href="https://codeberg.org/silkevicious/fediphoto-lineage/releases"><img src="https://silkevicious.codeberg.page/images/codeberg.png" height="80" alt="Codeberg badge"></a>
</p>
</td>
<td align="center" valign="center">
<p>
<a href="https://codeberg.org/silkevicious/apkrepo"><img src="https://silkevicious.codeberg.page/images/apkrepo.png" height="80" alt="ApkRepo badge"></a>
</p>
</td>
<td align="center" valign="center">
<p>
<a href="https://f-droid.org/packages/com.fediphoto.lineage/"><img src="https://silkevicious.codeberg.page/images/fdroid.png" height="80" alt="F-Droid badge"></a>
</p>
</td>
</tr>

</table>


For the original FediPhoto application see [HERE](https://github.com/pla1/FediPhoto#install)

## Testing Builds

We are testing the brand new Codeberg's CI.

Current build status: ![Building status](https://ci.codeberg.org/api/badges/silkevicious/fediphoto-lineage/status.svg)

## How-To

* connect **FediPhoto-Lineage** client with your _fediverse_ account (mastodon, pleroma, friendica and pixelfed).
* you'll be requested to concede this [permissions](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/PRIVACY.md)
* set up status configuration
* tap on main screen to open camera, take picture and click on check mark icon (bottom right)
* it's done!! FediPhoto-Lineage will publish your status and will receive a confirmation message afterwards

## Features

* Multiple fediverse account can be added
* Multiple status configurations can be set up in advance
* Preview before post or post immediately after press shoot
* Built-in location service (in case camera can't provide location)
* Offline mode (upload posts/photos when internet is back)
* Manage taken photos (keep or delete)
* Create OSM notes with your photos
* Import/Export settings and accounts


## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/6.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/7.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/8.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/9.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/10.png" width="150">


## Changelog

[FediPhoto-Lineage Changelog](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/CHANGELOG.md)

## License: GNU General Public License v3.0 only

```
Copyright (C) 2021 Sylke Vicious

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
```

## Privacy

[FediPhoto-Lineage Privacy Policy](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/PRIVACY.md)

**FediPhoto-Lineage** does NOT collect any personal data and/or information. It works locally on your device and information about your account is not shared or stored.

## Original Author

* [pla1](https://github.com/pla1)

## Developers

* [Sylke Vicious](https://codeberg.org/silkevicious)
* [කසුන්](https://codeberg.org/0xd9a)

## Bug Reports & Contributions

Please **open** a [issue](https://codeberg.org/silkevicious/fediphoto-lineage/issues) with detailed information about the problem.

If you're not comfortable to fill an issue or don't have an account here on Codeberg, reach out us at our [fediverse account](https://libranet.de/profile/fediphoto_lineage).

**Contribute** making feature requests and **code** contributions.

Contact translators for translation issues.

## Localization

We are now using Weblate to translate the UI and the F-Droid description files, to make life easier for our translator. Feel free to translate our app or request new languages by clicking the banner down here.

<a href="https://hosted.weblate.org/engage/fediphoto-lineage/">
<img src="https://hosted.weblate.org/widgets/fediphoto-lineage/-/open-graph.png" alt="Stato traduzione" />
</a>

### Contributors

* Italian
    * Sylke Vicious

* French
    * Sylke Vicious
    * mondstern
    * ButterflyOfFire
    * J. Lavoie
    * Maxime Leroy

* Spanish
    * Xosé M
    * tagomago

* Polish
    * mondstern

* Catalan
    * Sylke Vicious
    * spla
    * mondstern

* Arabic
    * ButterflyOfFire

* German
    * J. Lavoie
    * mondstern
    * Sylke Vicious

* Danish
    * mondstern

* Estonian
    * mondstern

* Basque
    * Izaro
    * Sylke Vicious
    * mondstern

* Galician
    * Xosé M

* Finnish
    * mondstern

* Dutch
    * mondstern
