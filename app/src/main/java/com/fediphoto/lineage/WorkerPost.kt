package com.fediphoto.lineage

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.database.QueueDAO
import com.fediphoto.lineage.database.ThreadDAO
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.toTemplate
import com.fediphoto.lineage.operations.ImageOperations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromStream
import kotlinx.serialization.json.encodeToJsonElement
import kotlinx.serialization.json.jsonPrimitive
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.math.BigInteger
import java.net.URL
import java.nio.charset.StandardCharsets
import java.util.Date
import java.util.Random
import javax.net.ssl.HttpsURLConnection

class WorkerPost(private val context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {
        val fplData = FPLData.getInstance(applicationContext)

        while (true) {
            val queueItem: QueueItem = fplData.getQueueDAO().getFirstPendingQueueItem() ?: break
            try {
                queueItem.mediaId ?: uploadMedia(fplData.getQueueDAO(), queueItem) ?: return Result.retry()
                postStatus(fplData.getQueueDAO(), fplData.getThreadDAO(), queueItem) ?: return Result.retry()
                queueItem.osmNoteText?.let { osmNoteText ->
                    val prefs = Prefs(applicationContext)
                    kotlin.runCatching {
                        osmCreateNote(
                            queueItem.osmToken,
                            queueItem.locationValue!!,
                            getFinalOSMNote(osmNoteText, queueItem, prefs.osmDirectLink, prefs.osmApplicationName)
                        )
                    }.onFailure {
                        it.printStackTrace()
                        fplData.getQueueDAO().update(queueItem.apply { error = it.localizedMessage })
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return Result.success()
    }

    private suspend fun uploadMedia(queueDAO: QueueDAO, queueItem: QueueItem): String? = withContext(Dispatchers.IO) {
        Log.i(TAG, String.format("Upload started %s", Date()))
        var mediaId: String? = null

        if (queueItem.photoPath.isBlank()) {
            val errorMessage = "File name is blank."
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            queueDAO.update(queueItem.apply { error = errorMessage })
            return@withContext null
        }

        var file = File(queueItem.photoPath)
        if (!file.exists()) {
            val errorMessage = "File \"${file.absoluteFile}\" does not exist."
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            queueDAO.update(queueItem.apply { error = errorMessage })
            return@withContext null
        }

        if (queueItem.removeExif) {
            val imageOperations = ImageOperations(context)
            val modifiedImage = File(context.filesDir, file.name)

            Log.i(TAG, "uploadMedia: 'Remove Exif' is enabled.")
            if (queueItem.exifRemoved) {
                Log.i(TAG, "Exif already removed.")
                file = modifiedImage
            } else {
                Log.i(TAG, "uploadMedia: Removing Exif...")
                val removeExifResult = imageOperations.removeExif(file, modifiedImage)
                if (removeExifResult.isSuccess) {
                    Log.i(TAG, "uploadMedia: Exif removed.")
                    file = modifiedImage
                } else {
                    Log.i(TAG, "uploadMedia: Failed to remove Exif")
                    modifiedImage.delete()
                    queueDAO.update(queueItem.apply {
                        error = removeExifResult.exceptionOrNull()?.localizedMessage ?: context.getString(R.string.error_exif_remove)
                    })
                    return@withContext null
                }
            }

            if (queueItem.compressImage) {
                Log.i(TAG, "uploadMedia: 'Compress Images' is enabled.")
                if (queueItem.imageCompressed) {
                    Log.i(TAG, "uploadMedia: Image is already compressed.")
                    file = modifiedImage
                } else {
                    Log.i(TAG, "uploadMedia: Compressing...")
                    val compressResult = imageOperations.compress(file, modifiedImage, queueItem.compressImageQuality)
                    if (compressResult.isSuccess) {
                        Log.i(TAG, "uploadMedia: Compressed.")
                        file = modifiedImage
                    } else {
                        Log.i(TAG, "uploadMedia: Failed to compress")
                        modifiedImage.delete()
                        queueDAO.update(queueItem.apply {
                            error = compressResult.exceptionOrNull()?.localizedMessage ?: context.getString(R.string.error_compress)
                        })
                        return@withContext null
                    }
                }
            }

            queueDAO.update(queueItem.apply { modifiedImagePath = modifiedImage.absolutePath })
        }

        Log.i(TAG, "Upload account: ${queueItem.accountId} Instance: ${queueItem.instance} Token: ${queueItem.token}")
        val boundary = BigInteger(256, Random()).toString()
        val urlString = "https://${queueItem.instance}/api/v1/media"
        val urlConnection: HttpsURLConnection
        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null
        var writer: PrintWriter? = null
        val responseBody = StringBuilder()
        try {
            val url = URL(urlString)
            Log.i(TAG, "URL: $url")
            urlConnection = url.openConnection() as HttpsURLConnection
            urlConnection.setRequestProperty("Cache-Control", "no-cache")
            urlConnection.setRequestProperty("User-Agent", "FediPhoto-Lineage")
            urlConnection.useCaches = false
            urlConnection.requestMethod = "POST"
            urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")
            urlConnection.doOutput = true
            // test
            val authorization = "Bearer ${queueItem.token}"
            urlConnection.setRequestProperty("Authorization", authorization)
            // end test
            outputStream = urlConnection.outputStream
            writer = PrintWriter(OutputStreamWriter(outputStream, StandardCharsets.UTF_8), true)
            writer.append("--").append(boundary).append(LINE_FEED)
            writer.append("Content-Disposition: form-data; name=\"access_token\"").append(LINE_FEED)
            writer.append("Content-Type: text/plain; charset=UTF-8").append(LINE_FEED)
            writer.append(LINE_FEED).append(queueItem.token).append(LINE_FEED).flush()

            queueItem.description?.let {
                writer.append("--").append(boundary).append(LINE_FEED)
                writer.append("Content-Disposition: form-data; name=\"description\"").append(LINE_FEED)
                writer.append("Content-Type: text/plain; charset=UTF-8").append(LINE_FEED)
                writer.append(LINE_FEED).append(it).append(LINE_FEED).flush()
            }

            writer.append("--").append(boundary).append(LINE_FEED)
            writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"")
            writer.append(file.name).append("\"").append(LINE_FEED)
            writer.append("Content-Type: image/jpeg").append(LINE_FEED)
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED)
            writer.append(LINE_FEED).flush()
            inputStream = FileInputStream(file)
            val buffer = ByteArray(4096)
            var bytesRead: Int
            while (inputStream.read(buffer).also { bytesRead = it } != -1) {
                outputStream.write(buffer, 0, bytesRead)
            }
            outputStream.flush()
            inputStream.close()

            //     writer.append(LINE_FEED).flush();
            writer.append(LINE_FEED).flush()
            writer.append("--").append(boundary).append("--").append(LINE_FEED)
            writer.close()
            outputStream.flush()
            Log.i(TAG, "Upload response code: ${urlConnection.responseCode}")
            urlConnection.instanceFollowRedirects = true
            inputStream = urlConnection.inputStream
            val isr = InputStreamReader(inputStream)
            val reader = BufferedReader(isr)
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                responseBody.append(line)
            }
            reader.close()
            urlConnection.disconnect()
            val jsonObject: JsonObject = Json.decodeFromString(responseBody.toString())
            Log.i(TAG, String.format("Upload output from upload: %s", jsonObject))
            queueItem.mediaId = jsonObject["id"]?.jsonPrimitive?.content ?: ""
            queueItem.mediaUrl = jsonObject["url"]?.jsonPrimitive?.content ?: ""
            mediaId = queueItem.mediaId
            queueDAO.update(queueItem)
        } catch (e: Exception) {
            System.out.format("Error: %s\nResponse body: %s\n", e.localizedMessage, responseBody)
            e.printStackTrace()
            queueDAO.update(queueItem.apply { error = e.localizedMessage })
        } finally {
            inputStream?.close()
            outputStream?.close()
            writer?.close()
        }

        return@withContext mediaId
    }

    private suspend fun postStatus(queueDAO: QueueDAO, threadDAO: ThreadDAO, queueItem: QueueItem): String? = withContext(Dispatchers.IO) {
        var statusId: String? = null

        if (queueItem.photoPath.isBlank()) {
            val errorMessage = context.getString(R.string.photo_file_name_blank)
            queueDAO.update(queueItem.apply { error = errorMessage })
            Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
            return@withContext null
        }

        val file = File(queueItem.photoPath)
        if (!file.exists()) {
            val errorMessage = "Photo file ${queueItem.photoPath} does not exist."
            queueDAO.update(queueItem.apply { error = errorMessage })
            Log.i(TAG, errorMessage)
            return@withContext null
        }

        Log.i(TAG, "Location: ${queueItem.locationValue}")
        Log.i(TAG, "Date: ${queueItem.dateValue}")

        val statusContent = queueItem.modifiedContent ?: createContent(
            queueItem.toTemplate(), queueItem.dateValue, queueItem.locationValue
        )

        val params = mutableMapOf<String, JsonElement>()
        params["status"] = Json.encodeToJsonElement(statusContent)
        params["access_token"] = Json.encodeToJsonElement(queueItem.token)
        params["visibility"] = Json.encodeToJsonElement(queueItem.visibility.urlValue)
        if (queueItem.sensitiveMedia)
            params["sensitive"] = Json.encodeToJsonElement(queueItem.visibility.urlValue)
        if (queueItem.spoilerEnabled && queueItem.spoilerText.isNotBlank())
            params["spoiler_text"] = Json.encodeToJsonElement(queueItem.spoilerText)
        params["media_ids"] = Json.encodeToJsonElement(listOf(queueItem.mediaId))
        params["client_name"] = Json.encodeToJsonElement("FediPhoto-Lineage")

        val statusThread: StatusThread? = threadDAO.get(queueItem.accountId, queueItem.templateId)
        val formattedDate = getFormattedDate()
        if (statusThread != null && (queueItem.threading == Threading.ALWAYS || queueItem.threading == Threading.DAILY && formattedDate == statusThread.statusDate)) {
            params["in_reply_to_id"] = Json.encodeToJsonElement(statusThread.statusId)
            Log.i(TAG, "${queueItem.threading} threading ID: ${statusThread.statusId} set to in_reply_to_id.")
        }

        val urlString = "https://${queueItem.instance}/api/v1/statuses"
        Log.i(TAG, "URL $urlString")
        val urlConnection: HttpsURLConnection
        var inputStream: InputStream? = null
        var outputStream: OutputStream? = null
        try {
            val url = URL(urlString)
            urlConnection = url.openConnection() as HttpsURLConnection
            urlConnection.setRequestProperty("Cache-Control", "no-cache")
            urlConnection.setRequestProperty("Accept", "application/json")
            urlConnection.setRequestProperty("User-Agent", "FediPhoto-Lineage")
            urlConnection.useCaches = false
            urlConnection.requestMethod = "POST"
            urlConnection.setRequestProperty("Content-type", "application/json; charset=UTF-8")
            urlConnection.doOutput = true
            val authorization = "Bearer ${queueItem.token}"
            urlConnection.setRequestProperty("Authorization", authorization)
            val json = Json.encodeToString(JsonObject(params))
            Log.i(TAG, "Posting JSON: $json")
            //     urlConnection.setRequestProperty("Content-length", Integer.toString(json.length()));
            outputStream = urlConnection.outputStream
            outputStream.write(json.toByteArray())
            outputStream.flush()
            val responseCode = urlConnection.responseCode
            Log.i(TAG, "Response code: $responseCode\n")
            urlConnection.instanceFollowRedirects = true
            inputStream = urlConnection.inputStream

            @OptIn(ExperimentalSerializationApi::class)
            val jsonObjectFromPost: JsonObject = Json.decodeFromStream(inputStream)

            Log.v(TAG, "Output: $jsonObjectFromPost")
            queueItem.statusId = jsonObjectFromPost["id"]?.jsonPrimitive?.content ?: ""
            queueItem.statusUrl = jsonObjectFromPost["url"]?.jsonPrimitive?.content ?: ""
            statusId = queueItem.statusId
            queueDAO.update(queueItem)
            sendNotification(context.getString(R.string.post_success), queueItem.statusUrl, queueItem.photoPath)
            File(queueItem.photoPath).delete()
            queueItem.modifiedImagePath?.let { File(it) }?.delete()
            threadingMaintenanceAfterPost(threadDAO, queueItem, statusThread)
        } catch (e: Exception) {
            e.printStackTrace()
            queueDAO.update(queueItem.apply { error = e.localizedMessage })
        } finally {
            inputStream?.close()
            outputStream?.close()
        }

        return@withContext statusId
    }

    private suspend fun threadingMaintenanceAfterPost(threadDAO: ThreadDAO, queueItem: QueueItem, thread: StatusThread?) {
        Log.i(TAG, "Threading: ${queueItem.threading} | Status id: ${queueItem.statusId}")
        if (queueItem.threading == Threading.NEVER)
            thread?.let { threadDAO.delete(it) }
        else {
            val formattedDate = getFormattedDate()
            if (queueItem.threading == Threading.ALWAYS)
                threadDAO.insert(StatusThread(queueItem.accountId, queueItem.templateId, queueItem.statusId!!, formattedDate))
            else if (queueItem.threading == Threading.DAILY) {
                if (thread == null)
                    threadDAO.insert(StatusThread(queueItem.accountId, queueItem.templateId, queueItem.statusId!!, formattedDate))
                else if (formattedDate != thread.statusDate)
                    threadDAO.update(thread.apply {
                        statusId = queueItem.statusId!!
                        statusDate = formattedDate
                    })
            }
        }
    }

    private fun sendNotification(title: String, urlString: String?, photoFileName: String?) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(urlString)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent = PendingIntent.getActivity(
            applicationContext, 0, intent,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else 0
        )
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        val notification = NotificationCompat.Builder(applicationContext, "default")
            .setContentTitle(title)
            .setContentText(urlString)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.fediphoto_foreground)
            .setLargeIcon(BitmapFactory.decodeFile(photoFileName))
            .setAutoCancel(true)
        val random = Random()
        val id = random.nextInt(9999 - 1000) + 1000
        notificationManager.notify(id, notification.build())
    }

    private fun getFinalOSMNote(osmNoteText: String, queueItem: QueueItem, directLink: Boolean, applicationName: Boolean): String {
        var note = "$osmNoteText\n\nMedia attachment:\n${queueItem.statusUrl}"
        if (directLink) note += "\n\n(${queueItem.mediaUrl})"
        if (applicationName) note += "\n\nvia FediPhoto-Lineage"
        return note
    }

    companion object {
        const val TAG = "WorkerPost"
        private const val LINE_FEED = "\r\n"
    }
}
