package com.fediphoto.lineage

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.NotificationManagerCompat.IMPORTANCE_LOW
import androidx.core.location.LocationListenerCompat
import androidx.core.location.LocationManagerCompat
import androidx.core.location.LocationRequestCompat
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.LocationUpdate
import java.util.Date

class LocationService : Service() {
    private lateinit var locationManager: LocationManager
    private var locationUpdate: LocationUpdate? = null
    private val locationListener = LocationListenerCompat { locationUpdate = LocationUpdate(Location(it.latitude, it.longitude), Date().time) }
    private lateinit var notificationManager: NotificationManagerCompat
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private var onServiceStop: (() -> Unit)? = null
    var isRunning = false

    inner class LocalBinder : Binder() {
        fun getService(): LocationService = this@LocationService
    }

    override fun onCreate() {
        isRunning = false
        super.onCreate()
    }

    private val binder = LocalBinder()

    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val channelName = getString(R.string.notification_channel_name)
        notificationManager = NotificationManagerCompat.from(this)
        notificationManager.createNotificationChannel(NotificationChannelCompat.Builder(CHANNEL_ID, IMPORTANCE_LOW).setName(channelName).build())
        notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID).setOnlyAlertOnce(true).setSound(null)

        locationManager = getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager

        isRunning =
            if (intent?.action == ACTION_STOP) {
                addNotification(false)
                LocationManagerCompat.removeUpdates(locationManager, locationListener)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    stopForeground(STOP_FOREGROUND_REMOVE)
                } else {
                    @Suppress("DEPRECATION")
                    stopForeground(true)
                }
                if (this::notificationManager.isInitialized) notificationManager.cancelAll()
                onServiceStop?.invoke()
                stopSelf()
                false
            } else {
                addNotification(true)
                addLocationListener()
                true
            }

        return START_NOT_STICKY
    }

    private fun addNotification(starting: Boolean) {
        val stopServiceIntent = Intent(this, LocationService::class.java).setAction(ACTION_STOP)
        val pendingIntentFlags =
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) PendingIntent.FLAG_CANCEL_CURRENT
            else PendingIntent.FLAG_CANCEL_CURRENT + PendingIntent.FLAG_IMMUTABLE
        val pendingIntent = PendingIntent.getService(this, 0, stopServiceIntent, pendingIntentFlags)
        if (starting) {
            notificationBuilder.setOngoing(true)
            notificationBuilder.setContentTitle(getString(R.string.notification_title))
            notificationBuilder.setSmallIcon(R.drawable.ic_location_notification)
            notificationBuilder.addAction(NotificationCompat.Action(null, getString(R.string.location_action_stop), pendingIntent))
        }
        val notification = notificationBuilder.build()
        startForeground(1, notification)
    }

    @SuppressLint("MissingPermission")
    private fun addLocationListener() {
        if (locationManager.isProviderEnabled(GPS_PROVIDER)) {
            val locationRequest = LocationRequestCompat.Builder(LOCATION_UPDATE_INTERVAL * 1000)
                .setMinUpdateIntervalMillis(LOCATION_UPDATE_INTERVAL * 1000 - 2000)
                .setMaxUpdateDelayMillis(LOCATION_UPDATE_INTERVAL * 1000 + 2000)
                .build()
            LocationManagerCompat.requestLocationUpdates(locationManager, GPS_PROVIDER, locationRequest, locationListener, Looper.getMainLooper())
        } else Log.i("TAG", "getLocation: provider not available")
    }

    override fun onBind(intent: Intent): IBinder = binder

    fun getLocationUpdate(): LocationUpdate? = locationUpdate

    fun setOnServiceStop(onServiceStop: (() -> Unit)?) {
        this.onServiceStop = onServiceStop
    }

    override fun onDestroy() {
        isRunning = false
        super.onDestroy()
    }

    companion object {
        const val LOCATION_UPDATE_INTERVAL = 10L
        private const val CHANNEL_ID = "background_location"
        const val ACTION_STOP = "stop_service"
    }
}
