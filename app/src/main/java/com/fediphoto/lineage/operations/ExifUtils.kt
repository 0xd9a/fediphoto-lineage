/*
 * Copyright (c) 2023 Sylke Vicious
 *
 * This file is part of FediPhoto-Lineage.
 *
 * FediPhoto-Lineage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * FediPhoto-Lineage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FediPhoto-Lineage.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2018-2020 Juan García Basilio
 *
 * This file is part of Scrambled Exif.
 *
 * Scrambled Exif is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scrambled Exif is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scrambled Exif.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.fediphoto.lineage.operations

import android.content.Context
import android.mediautil.image.jpeg.LLJTran
import android.mediautil.image.jpeg.LLJTranException
import android.util.Log
import androidx.exifinterface.media.ExifInterface
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream

internal class ExifUtils(private val context: Context) {
    companion object {
        private const val TAG = "ExifUtils"
    }

    fun rotateImageAccordingToExifOrientation(srcFile: File, dstFile: File) {
        val operation = when (ExifInterface(srcFile).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
            ExifInterface.ORIENTATION_ROTATE_270 -> {
                Log.v(TAG, "Exif rotation 270°"); LLJTran.ROT_270
            }

            ExifInterface.ORIENTATION_ROTATE_180 -> {
                Log.v(TAG, "Exif rotation 180°"); LLJTran.ROT_180
            }

            ExifInterface.ORIENTATION_ROTATE_90 -> {
                Log.d(TAG, "Exif rotation 90°"); LLJTran.ROT_90
            }

            else -> 0
        }

        if (operation == 0) {
            srcFile.copyTo(dstFile)
            Log.d(TAG, "The image ($srcFile) doesn't need to be rotated. Skipping...")
            return
        }

        Log.d(TAG, "Trying to rotate image with LLJTran")

        val output = File(context.filesDir, "rotate.jpg")

        val rotated = try {
            val lljTran = LLJTran(srcFile)
            lljTran.read(LLJTran.READ_ALL, false) // This could throw an LLJTranException. I am not catching it for now... Let's see.
            lljTran.transform(operation, LLJTran.OPT_DEFAULTS or LLJTran.OPT_XFORM_ORIENTATION)
            BufferedOutputStream(FileOutputStream(output)).use { writer ->
                lljTran.save(writer, LLJTran.OPT_WRITE_ALL)
            }
            lljTran.freeMemory()
            true
        } catch (e: LLJTranException) {
            e.printStackTrace()
            Log.e(TAG, "Error occurred while trying to rotate image with LLJTrans (AndroidMediaUtil).")
            false
        }

        if (rotated) {
            output.renameTo(dstFile)
            Log.d(TAG, "Done rotating image")
        } else {
            output.delete()
        }
    }
}

fun byteArrayFromInts(vararg ints: Int) = ByteArray(ints.size) { pos -> ints[pos].toByte() }

fun Byte.toHexString(): String = "%02X".format(this)
