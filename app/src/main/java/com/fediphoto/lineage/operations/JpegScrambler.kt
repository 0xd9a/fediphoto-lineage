/*
 * Copyright (c) 2023 Sylke Vicious
 *
 * This file is part of FediPhoto-Lineage.
 *
 * FediPhoto-Lineage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * FediPhoto-Lineage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FediPhoto-Lineage.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2018-2021 Juan García Basilio
 * Copyright (c) 2019 Eric Cochran (see https://github.com/NightlyNexus/ExifDataRemover/blob/master/app/src/main/java/com/nightlynexus/exifdataremover/Activity.kt#L107)
 *
 * This file is part of Scrambled Exif.
 *
 * Scrambled Exif is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scrambled Exif is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scrambled Exif.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.fediphoto.lineage.operations

import android.content.Context
import android.util.Log
import okio.buffer
import okio.sink
import okio.source
import java.io.File

class JpegScrambler(private val context: Context) {
    companion object {
        private const val TAG = "JpegScrambler"
    }

    private val jpegSegmentMarker = 0xFF.toByte()
    private val jpegSkippableSegments =
        byteArrayFromInts(0xFE, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF)
    private val jpegStartOfStream = 0xDA.toByte()

    @ExperimentalUnsignedTypes
    fun scramble(srcFile: File, dstFile: File) {
        val tempImage = File(context.filesDir, "scrambling.jpg")

        tempImage.sink().buffer().use { sink ->
            srcFile.inputStream().source().buffer().use { source ->
                sink.write(
                    source,
                    2
                ) // This writes the first (empty) start of image segment FFD8 (actually, JPEG allows for segments without payload. This code isn't really (yet?) compatible with that).

                while (!source.exhausted()) {
                    var marker = source.readByte()
                    var segmentType = source.readByte()

                    if (marker != jpegSegmentMarker) {
                        Log.d(
                            TAG,
                            "Invalid JPEG. Expected an FF marker (${"%02x".format(marker)} != ${"%02x".format(jpegSegmentMarker)}). Will try to skip bytes until we find a JPEG marker and hope for the best"
                        )
                        while (marker != jpegSegmentMarker || segmentType == jpegSegmentMarker || segmentType == 0x00.toByte()) {
                            Log.v(TAG, "Skipping byte in malformed JPEG file")
                            marker = segmentType
                            segmentType = source.readByte()
                        }
                    }

                    val size = source.readShort().toUShort()
                    if (size < 2u) {
                        tempImage.delete()
                        throw Exception("Invalid JPEG: segment ${segmentType.toHexString()} has wrong size: $size (<2)")
                    }

                    if (jpegSkippableSegments.contains(segmentType)) {
                        // Skip all APPn (0xEn) and COM (0xFE) segments (See: https://en.wikipedia.org/wiki/JPEG_Image#Syntax_and_structure)
                        Log.d(TAG, "Skipping JPEG segment ${segmentType.toHexString()} (APPn or COM): $size bytes")
                        source.skip((size - 2u).toLong()) // The size counts the 2 bytes of the size itself, and we've already read these
                    } else {
                        sink.writeByte(marker.toInt())
                        sink.writeByte(segmentType.toInt())
                        sink.writeShort(size.toInt())

                        if (segmentType == jpegStartOfStream) {
                            // Hopefully there aren't any other segments after the SOS
                            sink.writeAll(source)
                        } else {
                            sink.write(source, (size - 2u).toLong()) // The size counts the 2 bytes of the size itself, and we've already read these
                        }
                    }
                }
            }
        }

        tempImage.renameTo(dstFile)
    }
}
