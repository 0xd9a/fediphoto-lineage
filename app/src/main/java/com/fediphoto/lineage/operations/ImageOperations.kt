package com.fediphoto.lineage.operations

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.File

class ImageOperations(private val context: Context) {
    fun removeExif(srcFile: File, dstFile: File): Result<Boolean> {
        val tempFile = File(context.filesDir, "temp.jpg")
        return try {
            ExifUtils(context).rotateImageAccordingToExifOrientation(srcFile, tempFile)
            @OptIn(ExperimentalUnsignedTypes::class)
            JpegScrambler(context).scramble(tempFile, dstFile)
            Result.success(true)
        } catch (e: Exception) {
            e.printStackTrace()
            Result.failure(e)
        } finally {
            tempFile.delete()
        }
    }

    fun compress(srcFile: File, dstFile: File, imageQuality: Int): Result<Boolean> {
        val tempFile = File(context.filesDir, "temp.jpg")
        return try {
            tempFile.outputStream().use { output ->
                val bitmap = BitmapFactory.decodeFile(srcFile.absolutePath)
                bitmap.compress(Bitmap.CompressFormat.JPEG, imageQuality, output)
                bitmap.recycle()
            }
            tempFile.copyTo(dstFile, true)
            Result.success(true)
        } catch (e: Exception) {
            e.printStackTrace()
            Result.failure(e)
        } finally {
            tempFile.delete()
        }
    }
}
