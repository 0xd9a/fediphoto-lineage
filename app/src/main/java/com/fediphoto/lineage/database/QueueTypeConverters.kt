package com.fediphoto.lineage.database

import androidx.room.TypeConverter
import com.fediphoto.lineage.datatypes.Location
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.Date

class QueueTypeConverters {
    @TypeConverter
    fun dateToLong(date: Date): Long = date.time

    @TypeConverter
    fun longToDate(long: Long): Date = Date(long)

    @TypeConverter
    fun locationToString(location: Location?): String? = location?.let { Json.encodeToString(it) }

    @TypeConverter
    fun stringToLocation(string: String?): Location? = string?.let { Json.decodeFromString(it) }
}
