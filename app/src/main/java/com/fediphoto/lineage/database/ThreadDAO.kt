package com.fediphoto.lineage.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.fediphoto.lineage.datatypes.StatusThread

@Dao
interface ThreadDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(thread: StatusThread)

    @Update
    suspend fun update(thread: StatusThread)

    @Delete
    suspend fun delete(thread: StatusThread): Int

    @Query("SELECT * FROM threads")
    suspend fun getAll(): List<StatusThread>

    @Query("SELECT * FROM threads WHERE account_id = :accountId AND template_id = :templateId")
    suspend fun get(accountId: Int, templateId: Int): StatusThread?

    @Query("DELETE FROM threads WHERE account_id = :accountId")
    suspend fun deleteByAccountId(accountId: Int)

    @Query("DELETE FROM threads WHERE template_id = :templateId")
    suspend fun deleteByTemplateId(templateId: Int)
}
