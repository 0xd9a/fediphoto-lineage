package com.fediphoto.lineage.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.fediphoto.lineage.datatypes.FediAccount

@Dao
interface AccountDAO {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(account: FediAccount): Long

    @Update
    suspend fun update(account: FediAccount)

    @Delete
    suspend fun delete(account: FediAccount)

    @Query("SELECT * FROM accounts")
    suspend fun getAll(): List<FediAccount>

    @Query("SELECT * FROM accounts WHERE id = :accountId")
    suspend fun getById(accountId: Int): FediAccount?

    @Query("SELECT * FROM accounts WHERE instance = :instance AND username = :username")
    suspend fun getByInfo(instance: String, username: String): FediAccount?

    @Query("SELECT * FROM accounts WHERE rowId = :rowId")
    suspend fun getByRowId(rowId: Long): FediAccount
}
