package com.fediphoto.lineage.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.fediphoto.lineage.datatypes.Template

@Dao
interface TemplateDAO {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(template: Template): Long

    @Update
    suspend fun update(template: Template)

    @Delete
    suspend fun delete(template: Template)

    @Query("SELECT * FROM templates")
    suspend fun getAll(): List<Template>

    @Query("SELECT * FROM templates WHERE id = :templateId")
    suspend fun getById(templateId: Int): Template?

    @Query("SELECT * FROM templates WHERE rowId = :rowId")
    suspend fun getByRowId(rowId: Long): Template

    @Query("SELECT * FROM templates WHERE name = :name")
    suspend fun getByName(name: String): Template?
}
