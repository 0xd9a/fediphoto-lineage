package com.fediphoto.lineage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template

@Database(entities = [FediAccount::class, Template::class, StatusThread::class, QueueItem::class], version = 3, exportSchema = false)
@TypeConverters(QueueTypeConverters::class)
abstract class FPLData : RoomDatabase() {
    abstract fun getAccountDAO(): AccountDAO
    abstract fun getTemplateDAO(): TemplateDAO
    abstract fun getThreadDAO(): ThreadDAO
    abstract fun getQueueDAO(): QueueDAO

    companion object {
        @Volatile
        private var fplData: FPLData? = null

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE queue ADD COLUMN modified_content TEXT")
                database.execSQL("ALTER TABLE queue ADD COLUMN description TEXT")
            }
        }

        private val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE queue ADD COLUMN remove_exif INTEGER DEFAULT 1")
                database.execSQL("ALTER TABLE queue ADD COLUMN exif_removed INTEGER DEFAULT 0")
                database.execSQL("ALTER TABLE queue ADD COLUMN compress_image INTEGER DEFAULT 0")
                database.execSQL("ALTER TABLE queue ADD COLUMN compress_image_quality INTEGER DEFAULT 80")
                database.execSQL("ALTER TABLE queue ADD COLUMN compressed INTEGER DEFAULT 0")
            }
        }

        fun getInstance(context: Context): FPLData {
            return fplData ?: synchronized(this) {
                fplData ?: return Room.databaseBuilder(context, FPLData::class.java, "database.sqlite")
                    .addMigrations(MIGRATION_1_2)
                    .addMigrations(MIGRATION_2_3)
                    .build().also { fplData = it }
            }
        }
    }
}
