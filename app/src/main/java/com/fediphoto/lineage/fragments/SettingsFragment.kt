package com.fediphoto.lineage.fragments

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.preference.CheckBoxPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SeekBarPreference
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.OSMAccount
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.AfterPostAction
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.getThemeMode
import com.fediphoto.lineage.showErrorDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.io.OutputStream
import java.util.Locale

class SettingsFragment : PreferenceFragmentCompat() {
    private lateinit var activityResultCreateDocument: ActivityResultLauncher<String>
    private lateinit var activityResultOpenDocument: ActivityResultLauncher<Array<String>>
    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }
    private var exportData = ExportData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityResultCreateDocument = registerForActivityResult(ActivityResultContracts.CreateDocument("application/json")) { uri ->
            uri?.let { writeExportFile(it) }
        }
        activityResultOpenDocument = registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
            uri?.let { readExportFile(it) }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        val localeListPreference = findPreference<ListPreference>(getString(R.string.keyLanguage))
        val localeEntriesArray = resources.getStringArray(R.array.locales)
        localeListPreference?.entries = localeEntriesArray.map { getLocaleDisplayName(it) }.toTypedArray()
        localeListPreference?.entryValues = localeEntriesArray
        localeListPreference?.summaryProvider = Preference.SummaryProvider<ListPreference> { getLocaleDisplayName(it.value) }
        localeListPreference?.setOnPreferenceChangeListener { _, newValue ->
            if (prefs.language != newValue) {
                prefs.language = newValue as String
                activity?.recreate()
            }
            false
        }
        val themePreference = findPreference<ListPreference>(getString(R.string.keyTheme))
        themePreference?.setOnPreferenceChangeListener { preference, newValue ->
            if (prefs.theme != newValue) {
                prefs.theme = newValue as String
                AppCompatDelegate.setDefaultNightMode(getThemeMode(requireActivity()))
                preference.setIcon(getThemeIcon(newValue))
            }
            true
        }
        themePreference?.setIcon(getThemeIcon(themePreference.value))
        val compressQualityPreference = findPreference<SeekBarPreference>(getString(R.string.keyCompressImageQuality))
        compressQualityPreference?.max = 100
        compressQualityPreference?.min = 0
        findPreference<Preference>(getString(R.string.keyOsmNotes))?.setOnPreferenceClickListener {
            findNavController().navigate(SettingsFragmentDirections.settingsToOsmSettings())
            false
        }
        findPreference<Preference>(getString(R.string.keyExport))?.setOnPreferenceClickListener {
            getExportImportDialog(false).show()
            false
        }
        findPreference<Preference>(getString(R.string.keyImport))?.setOnPreferenceClickListener {
            activityResultOpenDocument.launch(arrayOf("application/json"))
            false
        }
    }

    private fun getLocaleDisplayName(value: String): String =
        if (value == getString(R.string.valueLanguageSystem))
            getString(R.string.locale_use_system)
        else
            Locale(value).displayLanguage

    @DrawableRes
    private fun getThemeIcon(value: String): Int = when (value) {
        getString(R.string.valueThemeDark) -> R.drawable.ic_dark_mode
        getString(R.string.valueThemeLight) -> R.drawable.ic_light_mode
        else ->
            if (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES)
                R.drawable.ic_dark_mode
            else
                R.drawable.ic_light_mode
    }

    private fun getExportImportDialog(isImport: Boolean): AlertDialog {
        val items: Array<String> = arrayOf(
            getString(R.string.settings),
            getString(R.string.status_configs),
            getString(R.string.accounts)
        )
        val selectedItems = booleanArrayOf(true, true, false)
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(if (isImport) R.string.label_select_data_import else R.string.label_select_data_export)
            .setMultiChoiceItems(items, selectedItems) { _, which, isChecked -> selectedItems[which] = isChecked }
            .setPositiveButton(R.string.ok) { dialogInterface, _ ->
                lifecycleScope.launch(Dispatchers.IO) { if (isImport) importData(selectedItems) else exportData(selectedItems) }
                dialogInterface.dismiss()
            }
            .setNegativeButton(R.string.cancel) { dialogInterface, _ -> dialogInterface?.dismiss() }
            .create()
    }

    private fun writeExportFile(uri: Uri) {
        if (exportData.isNotEmpty()) {
            var stream: OutputStream? = null
            try {
                stream = requireContext().contentResolver.openOutputStream(uri)
                stream?.write(Json.encodeToString(exportData).toByteArray())
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                stream?.close()
            }
            exportData = ExportData()
        }
    }

    private fun readExportFile(uri: Uri) {
        exportData = ExportData()
        val jsonInstance = Json { ignoreUnknownKeys = true }

        var stream: InputStream? = null
        try {
            stream = requireContext().contentResolver.openInputStream(uri)
            val json = String(stream!!.readBytes())
            exportData = jsonInstance.decodeFromString(json)
        } catch (e: Exception) {
            e.printStackTrace()
            stream?.close()
        } finally {
            stream?.close()
        }

        getExportImportDialog(true).show()
    }

    @Serializable
    private data class ExportData(
        var preferences: Preferences? = null,
        var templates: List<Template>? = null,
        var accounts: List<FediAccount>? = null
    ) {
        @Serializable
        data class Preferences(
            val cameraOnStart: Boolean,
            val previewBeforePost: Boolean,
            var savePhoto: Boolean? = null,
            var theme: String,
            val language: String?,
            var introOnStart: Boolean? = null,
            val locationService: Boolean,
            val osmNotes: Boolean? = null,
            val osmAccount: OSMAccount? = null,
            val osmNoteBeforePhoto: Boolean? = null,
            val osmVisibilityOverrideEnabled: Boolean? = null,
            val osmVisibilityOverrideValue: Visibility? = null,
            val activeAccountId: Int,
            val activeTemplateId: Int,
            @Deprecated("Not used since v7") val introDisplayed: Boolean? = null,
            @Deprecated("Not used since v7") val afterPostAction: AfterPostAction? = null
        ) {
            init {
                /* For importing pre-v7 exports */
                theme = theme.lowercase()
                introDisplayed?.let { introOnStart = !it }
                afterPostAction?.let { savePhoto = it == AfterPostAction.COPY || it == AfterPostAction.MOVE }
            }
        }
    }

    private fun ExportData.isNotEmpty() = preferences != null || templates != null || accounts != null

    private suspend fun exportData(selectedItems: BooleanArray) {
        exportData = ExportData()

        if (selectedItems[0])
            exportData.preferences = ExportData.Preferences(
                prefs.cameraOnStart,
                prefs.previewBeforePost,
                prefs.savePhoto,
                prefs.theme,
                prefs.language,
                prefs.introOnStart,
                prefs.locationService,
                prefs.osmNotes,
                prefs.osmAccount,
                prefs.osmNoteBeforePhoto,
                prefs.osmVisibilityOverrideEnabled,
                prefs.osmVisibilityOverrideValue,
                prefs.activeAccountId,
                prefs.activeTemplateId
            )
        if (selectedItems[1]) exportData.templates = fplData.getTemplateDAO().getAll()
        if (selectedItems[2]) exportData.accounts = fplData.getAccountDAO().getAll()

        try {
            activityResultCreateDocument.launch("export.json")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private suspend fun importData(selectedItems: BooleanArray) {
        if (exportData.isNotEmpty()) {
            var newActiveAccountId: Int = prefs.activeAccountId
            var newActiveTemplateId: Int = prefs.activeTemplateId

            kotlin.runCatching {
                if (selectedItems[1])
                    exportData.templates?.forEach { template ->
                        val wasActiveTemplate: Boolean = template.id == exportData.preferences?.activeTemplateId
                        val rowId = fplData.getTemplateDAO().insert(template.apply { id = 0 })
                        if (wasActiveTemplate) newActiveTemplateId = fplData.getTemplateDAO().getByRowId(rowId).id
                    }
                if (selectedItems[2]) {
                    exportData.accounts?.forEach { account ->
                        val wasActiveAccount: Boolean = account.id == exportData.preferences?.activeAccountId
                        val rowId = fplData.getAccountDAO().insert(account.apply { id = 0 })
                        if (wasActiveAccount) newActiveAccountId = fplData.getAccountDAO().getByRowId(rowId).id
                    }
                    prefs.osmAccount = exportData.preferences?.osmAccount
                }
                if (selectedItems[0]) withContext(Dispatchers.Main) {
                    exportData.preferences?.let { savePreferences(it, newActiveAccountId, newActiveTemplateId) }
                }
            }.onFailure {
                it.printStackTrace()
                withContext(Dispatchers.Main) { showErrorDialog(requireContext(), it.localizedMessage) }
            }
            exportData = ExportData()
        }
    }

    private fun savePreferences(importedPrefs: ExportData.Preferences, newActiveAccountId: Int, newActiveTemplateId: Int) {
        findPreference<CheckBoxPreference>(getString(R.string.keyCameraOnStart))?.isChecked = importedPrefs.cameraOnStart
        findPreference<CheckBoxPreference>(getString(R.string.keyPreviewBeforePost))?.isChecked = importedPrefs.previewBeforePost
        importedPrefs.savePhoto?.let { findPreference<CheckBoxPreference>(getString(R.string.keySavePhoto))?.isChecked = it }

        var themeChanged = false
        val themePreference = findPreference<ListPreference>(getString(R.string.keyTheme))
        if (themePreference?.value != importedPrefs.theme) {
            themePreference?.value = importedPrefs.theme
            themeChanged = true
        }

        var localeChanged = false
        if (prefs.language != importedPrefs.language) {
            prefs.language = importedPrefs.language
            localeChanged = true
        }
        importedPrefs.introOnStart?.let { findPreference<CheckBoxPreference>(getString(R.string.keyIntroOnStart))?.isChecked = it }
        findPreference<CheckBoxPreference>(getString(R.string.keyLocationService))?.isChecked = importedPrefs.locationService
        importedPrefs.osmNotes?.let { prefs.osmNotes = it }
        importedPrefs.osmNoteBeforePhoto?.let { prefs.osmNoteBeforePhoto = it }
        importedPrefs.osmVisibilityOverrideEnabled?.let { prefs.osmVisibilityOverrideEnabled = it }
        importedPrefs.osmVisibilityOverrideValue?.let { prefs.osmVisibilityOverrideValue = it }

        prefs.activeAccountId = newActiveAccountId
        prefs.activeTemplateId = newActiveTemplateId

        if (themeChanged)
            AppCompatDelegate.setDefaultNightMode(getThemeMode(requireActivity()))
        else if (localeChanged)
            activity?.recreate()
    }
}
