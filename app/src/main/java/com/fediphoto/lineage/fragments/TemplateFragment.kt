package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuProvider
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.appLocale
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.database.TemplateDAO
import com.fediphoto.lineage.databinding.DialogTemplatePreviewBinding
import com.fediphoto.lineage.databinding.FragmentTemplateBinding
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.eiffelTowerLocation
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.Date

class TemplateFragment : Fragment() {
    private var _binding: FragmentTemplateBinding? = null
    private val binding: FragmentTemplateBinding get() = _binding!!

    private val args: TemplateFragmentArgs by navArgs()
    private val templateDAO: TemplateDAO by lazy { FPLData.getInstance(requireActivity().applicationContext).getTemplateDAO() }
    private var editingTemplate: Template? = null

    private val dateFormat: MutableLiveData<String?> = MutableLiveData()

    private var selectedVisibility: Visibility = Visibility.PRIVATE
    private var selectedThreading: Threading = Threading.DAILY

    private val previewDate = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch(Dispatchers.Main) { editingTemplate = templateDAO.getById(args.templateId) }
        dateFormat.value = editingTemplate?.dateFormat
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentTemplateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.addMenuProvider(
            object : MenuProvider {
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) = menuInflater.inflate(R.menu.menu_template, menu)
                override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
                    R.id.action_preview -> {
                        showPreview()
                        true
                    }

                    R.id.action_save -> {
                        save()
                        true
                    }

                    else -> false
                }
            },
            viewLifecycleOwner
        )
        dateFormat.observe(viewLifecycleOwner) { binding.dateFormat.setText(it) }
        setFragmentResultListener(HelpDateFragment.REQUEST_KEY) { requestKey, bundle ->
            if (requestKey == HelpDateFragment.REQUEST_KEY) {
                val newDateFormat = bundle.getString(HelpDateFragment.BUNDLE_KEY, Template.DEFAULT_DATE_FORMAT)
                dateFormat.value = newDateFormat
            }
        }
        binding.spoilerSwitch.setOnCheckedChangeListener { _, isChecked -> binding.spoilerText.isEnabled = isChecked }
        binding.dateFormat.doOnTextChanged { text, _, _, _ ->
            if (text.isNullOrBlank()) binding.dateFormat.setText(Template.DEFAULT_DATE_FORMAT)
        }
        binding.dateHelpButton.setOnClickListener {
            findNavController().navigate(
                TemplateFragmentDirections.templateToHelpDate(
                    if (binding.dateFormat.text.toString() != Template.DEFAULT_DATE_FORMAT) binding.dateFormat.text.toString() else null
                )
            )
        }
        binding.dateSwitch.setOnCheckedChangeListener { _, isChecked -> binding.dateFormat.isEnabled = isChecked }
        binding.locationFormat.doOnTextChanged { text, _, _, _ -> if (text.isNullOrBlank()) binding.locationFormat.setText(Template.DEFAULT_DATE_FORMAT) }
        binding.locationSwitch.setOnCheckedChangeListener { _, isChecked ->
            binding.locationFormat.isEnabled = isChecked
            binding.locationFormatContainer.helperText = if (isChecked) getString(R.string.location_info) else null
        }
        binding.locationFormatContainer.helperText = if (binding.locationSwitch.isChecked) getString(R.string.location_info) else null
        binding.visibilityPublic.setOnClickListener { selectedVisibility = Visibility.PUBLIC }
        binding.visibilityUnlisted.setOnClickListener { selectedVisibility = Visibility.UNLISTED }
        binding.visibilityFollowers.setOnClickListener { selectedVisibility = Visibility.PRIVATE }
        binding.visibilityDirect.setOnClickListener { selectedVisibility = Visibility.DIRECT }
        binding.threadingNever.setOnClickListener { selectedThreading = Threading.NEVER }
        binding.threadingDaily.setOnClickListener { selectedThreading = Threading.DAILY }
        binding.threadingAlways.setOnClickListener { selectedThreading = Threading.ALWAYS }

        initialize()
    }

    private fun initialize() {
        lifecycleScope.launch(Dispatchers.IO) {
            val template = templateDAO.getById(args.templateId)
            withContext(Dispatchers.Main) {
                if (template != null) {
                    binding.name.setText(template.name)
                    binding.spoilerText.setText(template.spoilerText)
                    binding.spoilerText.isEnabled = template.spoilerEnabled
                    binding.spoilerSwitch.isChecked = template.spoilerEnabled
                    binding.text.setText(template.text)
                    binding.sensitiveMediaSwitch.isChecked = template.sensitiveMedia
                    binding.dateSwitch.isChecked = template.date
                    selectedThreading = template.threading
                    when (selectedThreading) {
                        Threading.NEVER -> binding.threadingNever
                        Threading.DAILY -> binding.threadingDaily
                        Threading.ALWAYS -> binding.threadingAlways
                    }.isChecked = true
                    selectedVisibility = template.visibility
                    when (selectedVisibility) {
                        Visibility.PUBLIC -> binding.visibilityPublic
                        Visibility.UNLISTED -> binding.visibilityUnlisted
                        Visibility.PRIVATE -> binding.visibilityFollowers
                        Visibility.DIRECT -> binding.visibilityDirect
                    }.isChecked = true
                    binding.locationSwitch.isChecked = template.location
                    binding.locationFormat.isEnabled = template.location
                    binding.locationFormat.setText(template.locationFormat)
                } else {
                    binding.locationFormat.setText(Template.DEFAULT_LOCATION_FORMAT)
                    binding.visibilityFollowers.performClick()
                    binding.threadingDaily.performClick()
                }
            }
        }
    }

    private fun save() {
        binding.root.isEnabled = false
        lifecycleScope.launch(Dispatchers.IO) {
            val template = checkTemplate()
            val isNewTemplate = args.templateId == 0
            if (isNewTemplate && templateDAO.getByName(template.name) != null) {
                withContext(Dispatchers.Main) {
                    MaterialAlertDialogBuilder(requireContext())
                        .setMessage(getString(R.string.template_by_name_exists, template.name))
                        .setNeutralButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
                        .setOnDismissListener { binding.root.isEnabled = true }
                        .show()
                }
            } else {
                if (isNewTemplate) {
                    val rowId = templateDAO.insert(template)
                    val prefs = Prefs(requireContext())
                    if (prefs.activeTemplateId == 0) prefs.activeTemplateId = templateDAO.getByRowId(rowId).id
                } else templateDAO.update(template)
                withContext(Dispatchers.Main) { findNavController().popBackStack() }
            }

        }
    }

    private fun checkTemplate(): Template {
        var dateFormat = ""
        if (binding.dateSwitch.isChecked && binding.dateFormat.text.toString().isNotBlank()) {
            kotlin.runCatching { SimpleDateFormat(binding.dateFormat.text.toString(), appLocale).toPattern() }
                .onSuccess { dateFormat = it }
                .onFailure {
                    it.printStackTrace()
                    AlertDialog.Builder(requireContext()).apply {
                        var message = getString(R.string.invalid_date_format)
                        it.localizedMessage?.let { errorMessage -> message += "\n\n$errorMessage" }
                        setMessage(message)
                        setNeutralButton(getString(R.string.ok)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        create()
                        show()
                    }
                }
        }
        return Template(
            args.templateId,
            binding.name.text?.takeIf { it.isNotBlank() }?.toString() ?: getString(R.string.status_config),
            binding.spoilerText.text.toString(),
            binding.spoilerSwitch.isChecked,
            binding.text.text.toString(),
            binding.sensitiveMediaSwitch.isChecked,
            selectedVisibility,
            selectedThreading,
            binding.dateSwitch.isChecked,
            dateFormat,
            binding.locationSwitch.isChecked,
            binding.locationFormat.text.toString().let { text -> text.ifBlank { Template.DEFAULT_LOCATION_FORMAT } }
        )
    }

    private fun showPreview() {
        var dateFormat = Template.DEFAULT_DATE_FORMAT
        if (binding.dateSwitch.isChecked && binding.dateFormat.text.toString().isNotBlank()) {
            try {
                dateFormat = SimpleDateFormat(binding.dateFormat.text.toString(), appLocale).toPattern()
                binding.dateFormat.error = null
            } catch (e: Exception) {
                binding.dateFormat.error = e.localizedMessage
                e.printStackTrace()
            }
        }
        val template = Template(
            args.templateId,
            binding.name.text?.toString() ?: getString(R.string.status_config),
            binding.spoilerText.text.toString(),
            binding.spoilerSwitch.isChecked,
            binding.text.text.toString(),
            binding.sensitiveMediaSwitch.isChecked,
            Visibility.PRIVATE,
            Threading.DAILY,
            binding.dateSwitch.isChecked,
            dateFormat,
            binding.locationSwitch.isChecked,
            binding.locationFormat.text.toString().ifBlank { Template.DEFAULT_LOCATION_FORMAT }
        )
        val dialog = MaterialAlertDialogBuilder(requireContext()).create()
        val templatePreviewDialogBinding = DialogTemplatePreviewBinding.inflate(LayoutInflater.from(dialog.context))
        templatePreviewDialogBinding.templateView.setTemplate(template, previewDate, eiffelTowerLocation)
        templatePreviewDialogBinding.button.setOnClickListener { dialog.dismiss() }
        dialog.setView(templatePreviewDialogBinding.root)
        dialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
