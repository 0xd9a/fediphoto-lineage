package com.fediphoto.lineage.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.FragmentOsmNoteBinding
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.showPermissionDialog
import com.fediphoto.lineage.submitWorkerPost
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class OSMNoteFragment : Fragment() {
    private var _binding: FragmentOsmNoteBinding? = null
    private val binding: FragmentOsmNoteBinding get() = _binding!!

    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }

    private val args: OSMNoteFragmentArgs by navArgs()
    private val location: MutableLiveData<Location?> = MutableLiveData(null)
    private var locationServiceStartedHere = false
    private lateinit var locationPermissionResultLauncher: ActivityResultLauncher<String>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        locationPermissionResultLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) binding.locationRow.startAndBindLocationService()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentOsmNoteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        args.note?.let { binding.note.setText(it) }

        location.observe(viewLifecycleOwner) {
            val locationAvailable = it != null
            binding.buttonContinue.isVisible = locationAvailable
            binding.locationRow.isVisible = !locationAvailable
        }
        if (location.value == null) location.value = args.location?.let { Json.decodeFromString(it) }
        binding.photo.load(args.photoPath)

        if (location.value == null) {
            startLocationServiceObserver()
            binding.locationRow.initialize(this::hasLocationPermission, this::getLocationPermission, viewLifecycleOwner)
            binding.locationRow.setOnLocationServiceBindFailedListener {
                binding.locationRow.setOnLocationServiceBindFailedListener(null)
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.osm_note_missing_location_title)
                    .setMessage(R.string.osm_note_missing_location_message)
                    .setPositiveButton(R.string.start) { dialog, _ ->
                        binding.locationRow.startAndBindLocationService()
                        locationServiceStartedHere = true
                        dialog.dismiss()
                    }
                    .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                    .show()
            }
            binding.locationRow.bindLocationService()
        }

        binding.buttonContinue.setOnClickListener { proceed(binding.note.text?.toString()?.takeIf { it.isNotBlank() }) }
        binding.buttonSkip.setOnClickListener { proceed(null) }
    }

    private fun proceed(noteText: String?) {
        if (prefs.previewBeforePost) {
            if (prefs.previewBeforePost)
                findNavController()
                    .navigate(OSMNoteFragmentDirections.osmNoteToPreview(args.photoPath, location.value?.let { Json.encodeToString(it) }, noteText))
            else
                lifecycleScope.launch(Dispatchers.Main) {
                    submitWorkerPost(
                        context = requireContext(),
                        activeAccount = fplData.getAccountDAO().getById(prefs.activeAccountId)!!,
                        activeTemplate = fplData.getTemplateDAO().getById(prefs.activeTemplateId)!!,
                        queueDAO = fplData.getQueueDAO(),
                        photoPath = args.photoPath,
                        location = location.value,
                        modifiedSpoilerText = null,
                        modifiedSpoilerEnabled = null,
                        modifiedContent = null,
                        contentDescription = null,
                        modifiedSensitiveMedia = null,
                        modifiedVisibility = null,
                        removeExif = prefs.removeExif,
                        compressImage = prefs.compressImage,
                        compressImageQuality = prefs.imageQuality,
                        osmToken = prefs.osmAccount?.token,
                        osmNoteText = noteText,
                        osmOverrideVisibility = prefs.osmVisibilityOverrideValue.takeIf { prefs.osmVisibilityOverrideEnabled }
                    )
                }
        }
    }

    private fun startLocationServiceObserver() {
        binding.locationRow.latestLocation.observe(viewLifecycleOwner) {
            if (it != null) {
                binding.locationRow.isVisible = false
                binding.locationRow.latestLocation.removeObservers(viewLifecycleOwner)
                if (locationServiceStartedHere) binding.locationRow.stopLocationService()
                location.value = it
            }
        }
    }

    private fun hasLocationPermission() =
        ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    private fun getLocationPermission() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ->
                showPermissionDialog(
                    requireContext(), R.drawable.ic_location, R.string.location_permission, R.string.location_permission_reason,
                ) { locationPermissionResultLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION) }

            else -> locationPermissionResultLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (locationServiceStartedHere)
            binding.locationRow.stopLocationService()
        else
            binding.locationRow.unbindLocationService()
        _binding = null
    }
}
