package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.adapters.RecyclerViewItemSpacing
import com.fediphoto.lineage.adapters.ThreadsAdapter
import com.fediphoto.lineage.cleanUpThreads
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.FragmentListBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Date

class ThreadsListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val previewDate = Date()

    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.isVisible = false
        binding.list.setHasFixedSize(true)
        binding.list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.list.addItemDecoration(RecyclerViewItemSpacing(binding.list.context, DividerItemDecoration.VERTICAL))

        initialize()
    }

    private fun initialize() {
        lifecycleScope.launch(Dispatchers.IO) {
            cleanUpThreads(fplData)
            val threads: MutableList<StatusThread> = fplData.getThreadDAO().getAll().toMutableList()
            val accounts: MutableList<FediAccount> = threads.map { fplData.getAccountDAO().getById(it.accountId)!! }.toMutableList()
            val templates: MutableList<Template> = threads.map { fplData.getTemplateDAO().getById(it.templateId)!! }.toMutableList()
            withContext(Dispatchers.Main) {
                binding.list.adapter =
                    ThreadsAdapter(
                        threads,
                        accounts,
                        templates,
                        fplData.getAccountDAO().getById(prefs.activeAccountId),
                        fplData.getTemplateDAO().getById(prefs.activeTemplateId),
                        previewDate,
                        lifecycleScope,
                        fplData.getThreadDAO()
                    )
            }
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        initialize()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
