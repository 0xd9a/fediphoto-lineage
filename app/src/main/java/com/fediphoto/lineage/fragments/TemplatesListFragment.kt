package com.fediphoto.lineage.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.adapters.RecyclerViewItemSpacing
import com.fediphoto.lineage.adapters.TemplatesAdapter
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.FragmentListBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TemplatesListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!

    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.setHasFixedSize(true)
        binding.list.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.list.addItemDecoration(RecyclerViewItemSpacing(binding.list.context, DividerItemDecoration.VERTICAL))
        binding.button.text = getString(R.string.add_status)
        binding.button.setOnClickListener { findNavController().navigate(R.id.templates_to_template) }

        initialize()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) initialize()
    }

    private fun initialize() {
        lifecycleScope.launch(Dispatchers.IO) {
            val templates = fplData.getTemplateDAO().getAll().toMutableList()
            withContext(Dispatchers.Main) {
                binding.list.adapter =
                    TemplatesAdapter(
                        templates,
                        fplData,
                        prefs,
                        lifecycleScope,
                        this@TemplatesListFragment::editTemplate,
                        this@TemplatesListFragment::scrollListTo
                    )
            }
        }
    }

    private fun editTemplate(templateId: Int) = findNavController().navigate(TemplatesListFragmentDirections.templatesToTemplate(templateId))

    private fun scrollListTo(position: Int) = binding.list.smoothScrollToPosition(position)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
