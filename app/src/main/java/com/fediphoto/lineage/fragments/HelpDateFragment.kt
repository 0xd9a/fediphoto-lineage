package com.fediphoto.lineage.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.appLocale
import com.fediphoto.lineage.databinding.FragmentHelpDateBinding
import com.fediphoto.lineage.databinding.ListItemSimpledateformatBinding
import java.text.SimpleDateFormat
import java.util.Date

class HelpDateFragment : Fragment() {
    private var _binding: FragmentHelpDateBinding? = null
    private val binding: FragmentHelpDateBinding get() = _binding!!

    private val args: HelpDateFragmentArgs by navArgs()

    private enum class DateBuildMethod { GUIDED, CUSTOM }

    private var selectedMethod = DateBuildMethod.GUIDED
        set(value) {
            displayMethodContainer(value)
            field = value
        }

    private var selectedDatePattern = ""
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value
            binding.dateFormat.setText("$selectedDatePattern $selectedTimePattern")
        }
    private var selectedTimePattern = ""
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value
            binding.dateFormat.setText("$selectedDatePattern $selectedTimePattern")
        }

    private val datePatterns = listOf(
        "dd MMMM, yyyy",
        "MMMM dd, yyyy",
        "yyyy MMMM dd"
    )
    private val timePatterns = listOf(
        "HH:mm",
        "hh:mm a"
    )

    private val previewDate: Date = Date()

    private val dateFormatLetters = mapOf(
        "d" to R.string.day_in_month,           // 1
        "dd" to R.string.day_in_month,          // 01
        "M" to R.string.month_in_year,          // 7
        "MM" to R.string.month_in_year,         // 07
        "MMM" to R.string.month_in_year,        // Jul
        "MMMM" to R.string.month_in_year,       // July
        "y" to R.string.year,                   // 2021
        "yy" to R.string.year,                  // 21
        "H" to R.string.hour_in_day_0,          // 0
        "HH" to R.string.hour_in_day_0,         // 00
        "m" to R.string.minute_in_hour,         // 1
        "mm" to R.string.minute_in_hour,        // 01
        "s" to R.string.second_in_minute,       // 1
        "ss" to R.string.second_in_minute,      // 01
        "a" to R.string.am_pm_marker,           // PM
        "E" to R.string.day_name_in_week,       // Tue
        "EEEE" to R.string.day_name_in_week,    // Tuesday
        "z" to R.string.time_zone,              // GMT-08:00
        "zzzz" to R.string.time_zone,           // Pacific Standard Time
        "Z" to R.string.time_zone,              // -0800
        "D" to R.string.day_in_year,            // 189
        "k" to R.string.hour_in_day_1,          // 24
        "h" to R.string.hour_in_am_pm_1,        // 12
        "K" to R.string.hour_in_am_pm_0,        // 0
        "w" to R.string.week_in_year,           // 27
        "W" to R.string.week_in_month,          // 2
        "F" to R.string.day_of_week_in_month,   // 2
        "S" to R.string.millisecond,            // 978
        "G" to R.string.era_designator          // AD
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHelpDateBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.addMenuProvider(
            object : MenuProvider {
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) = menuInflater.inflate(R.menu.menu_help_date, menu)
                override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
                    R.id.action_save -> {
                        setFragmentResult(REQUEST_KEY, bundleOf(BUNDLE_KEY to binding.dateFormat.text.toString()))
                        findNavController().popBackStack()
                        true
                    }

                    else -> false
                }
            },
            viewLifecycleOwner
        )
        setGuidedMethodValues()

        binding.methodGuided.setOnClickListener { selectedMethod = DateBuildMethod.GUIDED }
        binding.methodCustom.setOnClickListener { selectedMethod = DateBuildMethod.CUSTOM }

        binding.guidedDate1.setOnClickListener { selectedDatePattern = datePatterns[0] }
        binding.guidedDate2.setOnClickListener { selectedDatePattern = datePatterns[1] }
        binding.guidedDate3.setOnClickListener { selectedDatePattern = datePatterns[2] }
        binding.guidedDate4.setOnClickListener { selectedDatePattern = "" }
        binding.guidedTime1.setOnClickListener { selectedTimePattern = timePatterns[0] }
        binding.guidedTime2.setOnClickListener { selectedTimePattern = timePatterns[1] }
        binding.guidedTime3.setOnClickListener { selectedTimePattern = "" }

        binding.dateFormat.doOnTextChanged { text, _, _, _ -> updatePreview(text.toString()) }
        dateFormatLetters.forEach { mapEntry ->
            val listItemViewBinding = ListItemSimpledateformatBinding.inflate(
                LayoutInflater.from(binding.customDateFormatHelp.context),
                binding.customDateFormatHelp,
                false
            )
            listItemViewBinding.key.text = mapEntry.key
            listItemViewBinding.description.text = getString(mapEntry.value)
            listItemViewBinding.value.text = SimpleDateFormat(mapEntry.key, appLocale).format(previewDate)
            listItemViewBinding.root.setOnClickListener {
                val cursorPosition = binding.dateFormat.selectionStart
                binding.dateFormat.text?.insert(cursorPosition, " ${mapEntry.key}")
            }
            binding.customDateFormatHelp.addView(listItemViewBinding.root)
        }

        val currentDatePattern = args.dateFormat ?: ""

        if (currentDatePattern.isBlank()) {
            binding.methodGuided.performClick()
            binding.guidedDate1.performClick()
            binding.guidedTime1.performClick()
        } else {
            binding.methodCustom.performClick()
            binding.dateFormat.setText(currentDatePattern)
        }
    }

    private fun setGuidedMethodValues() {
        binding.guidedDate1.text = SimpleDateFormat(datePatterns[0], appLocale).format(previewDate)
        binding.guidedDate2.text = SimpleDateFormat(datePatterns[1], appLocale).format(previewDate)
        binding.guidedDate3.text = SimpleDateFormat(datePatterns[2], appLocale).format(previewDate)
        binding.guidedTime1.text = SimpleDateFormat(timePatterns[0], appLocale).format(previewDate)
        binding.guidedTime2.text = SimpleDateFormat(timePatterns[1], appLocale).format(previewDate)
    }

    private fun displayMethodContainer(method: DateBuildMethod) {
        when (method) {
            DateBuildMethod.GUIDED -> {
                binding.guidedDateFormatContainer.isVisible = true
                binding.customDateFormatContainer.isVisible = false
            }

            DateBuildMethod.CUSTOM -> {
                binding.customDateFormatContainer.isVisible = true
                binding.guidedDateFormatContainer.isVisible = false
            }
        }
    }

    override fun onStop() {
        (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(binding.root.windowToken, 0)
        super.onStop()
    }

    private fun updatePreview(datePattern: String) {
        try {
            binding.preview.text = SimpleDateFormat(datePattern, appLocale).format(previewDate)
            binding.dateFormat.error = null
        } catch (e: Exception) {
            binding.dateFormat.error = e.localizedMessage
            e.printStackTrace()
        }
    }

    companion object {
        const val REQUEST_KEY = "date_pattern"
        const val BUNDLE_KEY = "date_pattern"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
