package com.fediphoto.lineage.fragments.intro

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.FragmentIntroPermissionsBinding
import com.fediphoto.lineage.olderThanQ

class PermissionIntroFragment : Fragment() {
    private lateinit var viewBinding: FragmentIntroPermissionsBinding
    private lateinit var permissionResultLauncher: ActivityResultLauncher<Array<String>>
    private var hasCameraPermission = false
        set(value) {
            viewBinding.camera.request.isVisible = !value
            viewBinding.camera.granted.isVisible = value
            field = value
        }
    private var hasStoragePermission = false
        set(value) {
            viewBinding.storage.request.isVisible = !value
            viewBinding.storage.granted.isVisible = value
            field = value
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        permissionResultLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            if (permissions.containsKey(Manifest.permission.CAMERA))
                hasCameraPermission = permissions[Manifest.permission.CAMERA] == true
            if (permissions.containsKey(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                hasStoragePermission = permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewBinding = FragmentIntroPermissionsBinding.inflate(inflater, container, false)

        hasCameraPermission = isPermissionGranted(Manifest.permission.CAMERA)

        if (olderThanQ) {
            hasStoragePermission = isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            viewBinding.containerStorage.isVisible = true
        } else hasStoragePermission = true

        viewBinding.nav.previous.setOnClickListener { findNavController().popBackStack() }
        viewBinding.nav.next.setOnClickListener { findNavController().navigate(PermissionIntroFragmentDirections.permissionToAccount()) }

        viewBinding.camera.icon.setImageResource(R.drawable.ic_camera)
        viewBinding.camera.name.setText(R.string.camera_permission)
        viewBinding.camera.description.setText(R.string.camera_permission_reason)
        viewBinding.storage.icon.setImageResource(R.drawable.ic_storage)
        viewBinding.storage.name.setText(R.string.storage_permission)
        viewBinding.storage.description.setText(R.string.storage_permission_reason)

        viewBinding.camera.request.setOnClickListener { permissionResultLauncher.launch(arrayOf(Manifest.permission.CAMERA)) }
        viewBinding.storage.request.setOnClickListener { permissionResultLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)) }

        return viewBinding.root
    }

    private fun isPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(requireContext(), permission) == PackageManager.PERMISSION_GRANTED
}
