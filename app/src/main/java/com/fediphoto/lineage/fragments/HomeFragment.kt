package com.fediphoto.lineage.fragments

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.TooltipCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.load
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.DialogOsmTakeNoteBinding
import com.fediphoto.lineage.databinding.FragmentHomeBinding
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.olderThanQ
import com.fediphoto.lineage.showPermissionDialog
import com.fediphoto.lineage.submitWorkerPost
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!

    private val prefs: Prefs by lazy { Prefs(requireActivity().applicationContext) }
    private val fplData: FPLData by lazy { FPLData.getInstance(requireActivity().applicationContext) }

    private var currentPhotoPath: String = ""
    private lateinit var takePicture: ActivityResultLauncher<Uri>

    private var osmNote: String? = null

    private var noAccountAlertDialog: AlertDialog? = null
    private var noTemplateAlertDialog: AlertDialog? = null

    private lateinit var permissionResultLauncher: ActivityResultLauncher<Array<String>>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        permissionResultLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            permissions.forEach {
                when (it.key) {
                    Manifest.permission.ACCESS_FINE_LOCATION -> if (it.value) binding.locationRow.startAndBindLocationService()
                    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE -> updateUI()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        takePicture = registerForActivityResult(ActivityResultContracts.TakePicture()) { successful ->
            Log.i(TAG, "Camera result: $successful")
            if (successful) {
                if (prefs.savePhoto)
                    lifecycleScope.launch(Dispatchers.IO) {
                        kotlin.runCatching { saveToExternal(currentPhotoPath) }.onFailure { it.printStackTrace() }
                    }
                var location: Location? = binding.locationRow.latestLocation.value
                if (location == null)
                    runCatching { ExifInterface(currentPhotoPath).latLong!! }
                        .onSuccess { latLong -> location = Location(latLong[0], latLong[1]) }
                        .onFailure { it.printStackTrace() }

                if (prefs.osmNotes)
                    findNavController()
                        .navigate(HomeFragmentDirections.homeToOsmNote(currentPhotoPath, location?.let { Json.encodeToString(it) }, osmNote))
                else if (prefs.previewBeforePost)
                    findNavController()
                        .navigate(HomeFragmentDirections.homeToPreview(currentPhotoPath, location?.let { Json.encodeToString(it) }, null))
                else
                    lifecycleScope.launch(Dispatchers.Main) {
                        submitWorkerPost(
                            context = requireContext(),
                            activeAccount = fplData.getAccountDAO().getById(prefs.activeAccountId)!!,
                            activeTemplate = fplData.getTemplateDAO().getById(prefs.activeTemplateId)!!,
                            queueDAO = fplData.getQueueDAO(),
                            photoPath = currentPhotoPath,
                            location = location,
                            modifiedSpoilerText = null,
                            modifiedSpoilerEnabled = null,
                            modifiedContent = null,
                            contentDescription = null,
                            modifiedSensitiveMedia = null,
                            modifiedVisibility = null,
                            removeExif = prefs.removeExif,
                            compressImage = prefs.compressImage,
                            compressImageQuality = prefs.imageQuality,
                            osmToken = null,
                            osmNoteText = null,
                            osmOverrideVisibility = prefs.osmVisibilityOverrideValue.takeIf { prefs.osmVisibilityOverrideEnabled })
                    }
            } else {
                osmNote = null
                File(currentPhotoPath).delete()
                Log.i(TAG, "Deleted: $currentPhotoPath")
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.locationRow.initialize(this::hasLocationPermission, this::getLocationPermission, viewLifecycleOwner)

        updateUI()

        binding.threadsButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToThreads()) }
        binding.pendingButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToQueue()) }
        binding.cameraButton.setOnClickListener {
            if (prefs.osmNotes && prefs.osmNoteBeforePhoto)
                doOrRequestMissingRequirements { showTakeNoteDialog() }
            else
                doOrRequestMissingRequirements { proceedToCamera() }
        }

        doOrRequestMissingRequirements { }

        if (prefs.cameraOnStart && prefs.canAutoOpenCamera) binding.cameraButton.performClick()
        if (prefs.locationService) binding.locationRow.bindLocationService()

        prefs.canAutoOpenCamera = false
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden) updateUI()
        super.onHiddenChanged(hidden)
    }

    private fun updateUI() {
        val cameraTextRes =
            if (prefs.osmNotes)
                if (prefs.osmNoteBeforePhoto)
                    R.string.write_a_new_note
                else
                    R.string.post_with_preview
            else
                if (prefs.previewBeforePost)
                    R.string.post_with_preview
                else
                    R.string.post_without_preview

        binding.cameraText.text = getString(cameraTextRes)

        lifecycleScope.launch(Dispatchers.IO) {
            fplData.getAccountDAO().getById(prefs.activeAccountId).let { activeAccount ->
                if (activeAccount != null) {
                    val displayName = activeAccount.displayName.ifBlank { activeAccount.username }
                    val username = "@${activeAccount.username}@${activeAccount.instance}"
                    binding.accountImage.load(activeAccount.avatar) { placeholder(R.drawable.ic_account) }
                    withContext(Dispatchers.Main) {
                        binding.accountDisplayName.text = displayName
                        binding.accountUsername.text = username
                        binding.accountUsername.isVisible = true
                        binding.accountsButton.contentDescription = getString(R.string.accounts)
                        TooltipCompat.setTooltipText(binding.accountsButton, getString(R.string.accounts))
                        binding.accountsButton.setIconResource(R.drawable.ic_list)
                        binding.accountsButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToAccounts()) }
                    }
                } else withContext(Dispatchers.Main) {
                    binding.accountDisplayName.text = getString(R.string.add_account)
                    binding.accountUsername.isVisible = false
                    binding.accountsButton.contentDescription = getString(R.string.add_account)
                    TooltipCompat.setTooltipText(binding.accountsButton, getString(R.string.add_account))
                    binding.accountsButton.setIconResource(R.drawable.ic_add)
                    binding.accountsButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToLogin()) }
                    binding.accountImage.setImageResource(R.drawable.ic_account)
                }
            }
        }

        lifecycleScope.launch(Dispatchers.IO) {
            fplData.getTemplateDAO().getById(prefs.activeTemplateId).let { activeTemplate ->
                if (activeTemplate != null) withContext(Dispatchers.Main) {
                    binding.templateName.text = activeTemplate.name
                    binding.templatesButton.contentDescription = getString(R.string.status_configs)
                    TooltipCompat.setTooltipText(binding.templatesButton, getString(R.string.status_configs))
                    binding.templatesButton.setIconResource(R.drawable.ic_list)
                    binding.templatesButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToTemplates()) }
                } else withContext(Dispatchers.Main) {
                    binding.templateName.text = getString(R.string.add_status)
                    binding.templatesButton.contentDescription = getString(R.string.add_status)
                    TooltipCompat.setTooltipText(binding.templatesButton, getString(R.string.add_status))
                    binding.templatesButton.setIconResource(R.drawable.ic_add)
                    binding.templatesButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToTemplate()) }
                }
            }
        }
        binding.osmNotesButton.isVisible = prefs.osmNotes
        if (prefs.osmNotes) binding.osmNotesButton.setOnClickListener { findNavController().navigate(HomeFragmentDirections.homeToOsmSettings()) }

        if (prefs.locationService) {
            binding.locationRow.bindLocationService()
            binding.locationRow.isVisible = true
        } else {
            binding.locationRow.isVisible = false
        }
    }

    private fun showTakeNoteDialog() {
        osmNote = null
        val dialogBinding = DialogOsmTakeNoteBinding.inflate(LayoutInflater.from(requireContext()))
        MaterialAlertDialogBuilder(requireContext())
            .setView(dialogBinding.root)
            .setPositiveButton(R.string.continue_) { dialog, _ ->
                osmNote = dialogBinding.note.text?.takeIf { it.isNotBlank() }?.toString()
                proceedToCamera()
                dialog.dismiss()
            }
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun proceedToCamera() {
        createPhotoFile(requireContext())?.let { file ->
            val photoUri = FileProvider.getUriForFile(requireContext(), "com.fediphoto.lineage.fileprovider", file)
            currentPhotoPath = file.absolutePath
            Log.i(TAG, "takePicture: photoUri: $photoUri")
            takePicture.launch(photoUri)
        }
    }

    private fun saveToExternal(photoPath: String) {
        try {
            val sourceFile = File(photoPath)
            if (!sourceFile.exists()) return
            val source = FileInputStream(sourceFile).channel
            val fos: OutputStream? =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    val resolver = requireContext().contentResolver
                    val contentValues = ContentValues()
                    contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, sourceFile.name + ".jpg")
                    contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                    val imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                    imageUri?.let { resolver.openOutputStream(it) }
                } else {
                    val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
                    val image = File(imagesDir, sourceFile.name + ".jpg")
                    FileOutputStream(image)
                }
            val buffer = ByteArray(1024)
            var bytesRead: Int
            while (source.read(ByteBuffer.wrap(buffer)).also { bytesRead = it } != -1)
                fos?.write(buffer, 0, bytesRead)
            fos?.close()
            source.close()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun doOrRequestMissingRequirements(action: () -> Unit) {
        lifecycleScope.launch(Dispatchers.Main) {
            when {
                fplData.getAccountDAO().getById(prefs.activeAccountId) == null -> noAccountAlert()
                fplData.getTemplateDAO().getById(prefs.activeTemplateId) == null -> noTemplateAlert()
                !hasCameraPermissions() -> getCameraPermission()
                !hasStoragePermission() -> getStoragePermission()
                else -> action()
            }
        }
    }

    private fun hasCameraPermissions() = isPermissionGranted(Manifest.permission.CAMERA)
    private fun getCameraPermission() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ->
                showPermissionDialog(
                    requireContext(), R.drawable.ic_camera, R.string.camera_permission, R.string.camera_permission_reason
                ) { permissionResultLauncher.launch(arrayOf(Manifest.permission.CAMERA)) }

            else -> permissionResultLauncher.launch(arrayOf(Manifest.permission.CAMERA))
        }
    }

    private fun hasStoragePermission() = !olderThanQ || (olderThanQ && isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE))
    private fun getStoragePermission() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) ->
                showPermissionDialog(
                    requireContext(), R.drawable.ic_storage, R.string.storage_permission, R.string.storage_permission_reason
                ) { permissionResultLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)) }

            else -> permissionResultLauncher.launch(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))
        }
    }

    private fun hasLocationPermission() = isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)
    private fun getLocationPermission() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ->
                showPermissionDialog(
                    requireContext(), R.drawable.ic_location, R.string.location_permission, R.string.location_permission_reason
                ) { permissionResultLauncher.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)) }

            else -> permissionResultLauncher.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
        }
    }

    private fun isPermissionGranted(permission: String) =
        ContextCompat.checkSelfPermission(requireActivity(), permission) == PackageManager.PERMISSION_GRANTED

    private fun noAccountAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.add_account))
            .setMessage(getString(R.string.no_account))
            .setPositiveButton(getString(R.string.add_account)) { dialog, _ ->
                findNavController().navigate(HomeFragmentDirections.homeToLogin())
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun noTemplateAlert() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.add_status))
            .setMessage(getString(R.string.no_config))
            .setPositiveButton(getString(R.string.add_status)) { dialog, _ ->
                findNavController().navigate(HomeFragmentDirections.homeToTemplate())
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun createPhotoFile(context: Context): File? {
        var file: File? = null
        val fileName = "photo_" + SimpleDateFormat("yyyyMMdd_HHmmss_", Locale.ENGLISH).format(Date())
        Log.i(TAG, "createPhotoFile: fileName: $fileName")

        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        Log.i(TAG, "createPhotoFile: storageDir: $storageDir")

        if (storageDir != null) {
            if (!storageDir.exists()) {
                Log.w(TAG, "createPhotoFile: storageDir does not exist. Trying to create")
                if (storageDir.mkdir())
                    Log.i(TAG, "createPhotoFile: create storageDir - Successful")
                else
                    Log.e(TAG, "createPhotoFile: create storageDir - Failed")
            }

            if (storageDir.exists()) file = File.createTempFile(fileName, ".jpg", storageDir)

        } else Log.e(TAG, "createPhotoFile: Shared storage is not currently available")

        return file
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.locationRow.unbindLocationService()
        _binding = null
    }

    override fun onDestroy() {
        noAccountAlertDialog?.cancel()
        noTemplateAlertDialog?.cancel()
        super.onDestroy()
    }

    companion object {
        private const val TAG: String = "HomeFragment"
    }
}
