package com.fediphoto.lineage.datatypes.enums

@Deprecated("Not used since v7")
enum class AfterPostAction {
    LEAVE,
    COPY,
    MOVE,
    DELETE
}