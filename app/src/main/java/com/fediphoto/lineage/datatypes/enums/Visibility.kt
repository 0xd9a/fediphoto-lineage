package com.fediphoto.lineage.datatypes.enums

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.fediphoto.lineage.R

enum class Visibility(@StringRes val stringId: Int, @DrawableRes val drawableId: Int) {
    PUBLIC(R.string.visibility_public, R.drawable.ic_public),
    UNLISTED(R.string.visibility_unlisted, R.drawable.ic_unlisted),
    PRIVATE(R.string.visibility_followers, R.drawable.ic_followers),
    DIRECT(R.string.visibility_direct, R.drawable.ic_direct);

    val urlValue = name.lowercase()
}
