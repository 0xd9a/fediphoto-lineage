package com.fediphoto.lineage.datatypes

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FediApplication(
    @SerialName("redirect_uri") var redirectUri: String,
    @SerialName("client_id") var clientId: String,
    @SerialName("client_secret") var clientSecret: String
)
