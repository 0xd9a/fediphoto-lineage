package com.fediphoto.lineage.datatypes.enums

enum class QueueState { PENDING, IN_PROGRESS, DONE, FAILED, RETRYING }
