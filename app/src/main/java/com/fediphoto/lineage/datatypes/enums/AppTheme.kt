package com.fediphoto.lineage.datatypes.enums

@Deprecated("Not used since v7")
enum class AppTheme {
    SYSTEM,
    LIGHT,
    DARK
}
