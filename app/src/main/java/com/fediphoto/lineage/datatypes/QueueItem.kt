package com.fediphoto.lineage.datatypes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fediphoto.lineage.datatypes.enums.QueueState
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility
import java.util.Date

@Entity(tableName = "queue")
data class QueueItem(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val instance: String,
    val token: String,
    @ColumnInfo(name = "photo_path") val photoPath: String,
    @ColumnInfo(name = "spoiler_text") val spoilerText: String,
    @ColumnInfo(name = "spoiler_enabled") val spoilerEnabled: Boolean,
    val text: String,
    val description: String?,
    @ColumnInfo(name = "sensitive_media") val sensitiveMedia: Boolean,
    val visibility: Visibility,
    val threading: Threading,
    val date: Boolean,
    @ColumnInfo(name = "date_format") val dateFormat: String,
    @ColumnInfo(name = "date_value") val dateValue: Date?,
    val location: Boolean,
    @ColumnInfo(name = "location_format") val locationFormat: String,
    @ColumnInfo(name = "location_value") val locationValue: Location?,
    @ColumnInfo(name = "modified_content") val modifiedContent: String?,
    @ColumnInfo(name = "account_id") val accountId: Int,
    @ColumnInfo(name = "template_id") val templateId: Int,
    @ColumnInfo(name = "remove_exif") val removeExif: Boolean,
    @ColumnInfo(name = "exif_removed") val exifRemoved: Boolean,
    @ColumnInfo(name = "compress_image") val compressImage: Boolean,
    @ColumnInfo(name = "compress_image_quality") val compressImageQuality: Int,
    @ColumnInfo(name = "image_compressed") val imageCompressed: Boolean,
    var state: QueueState,
    @ColumnInfo(name = "modified_image_path") var modifiedImagePath: String? = null,
    @ColumnInfo(name = "media_id") var mediaId: String? = null,
    @ColumnInfo(name = "media_url") var mediaUrl: String? = null,
    @ColumnInfo(name = "status_id") var statusId: String? = null,
    @ColumnInfo(name = "status_url") var statusUrl: String? = null,
    @ColumnInfo(name = "osm_token") val osmToken: String? = null,
    @ColumnInfo(name = "osm_note_text") val osmNoteText: String? = null,
    var error: String? = null
)

fun QueueItem.toTemplate() = Template(
    0, "", spoilerText, spoilerEnabled, text, sensitiveMedia, visibility, threading, date, dateFormat, location, locationFormat
)
