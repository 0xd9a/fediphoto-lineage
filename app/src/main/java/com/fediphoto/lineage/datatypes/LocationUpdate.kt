package com.fediphoto.lineage.datatypes

import kotlinx.serialization.Serializable

@Serializable
data class LocationUpdate(
    val location: Location,
    val time: Long
)
