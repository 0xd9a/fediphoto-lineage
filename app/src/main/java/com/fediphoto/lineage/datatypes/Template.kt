package com.fediphoto.lineage.datatypes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = "templates")
data class Template(
    @PrimaryKey(autoGenerate = true) var id: Int,
    var name: String,
    @ColumnInfo(name = "spoiler_text") val spoilerText: String = "",
    @ColumnInfo(name = "spoiler_enabled") val spoilerEnabled: Boolean = false,
    val text: String,
    @ColumnInfo(name = "sensitive_media") val sensitiveMedia: Boolean = false,
    val visibility: Visibility,
    val threading: Threading = Threading.DAILY,
    val date: Boolean,
    @ColumnInfo(name = "date_format") val dateFormat: String,
    val location: Boolean,
    @ColumnInfo(name = "location_format") val locationFormat: String = DEFAULT_LOCATION_FORMAT
) {
    companion object {
        const val PATTERN_LOCATION_LATITUDE = "%lt"
        const val PATTERN_LOCATION_LONGITUDE = "%ln"
        const val DEFAULT_LOCATION_FORMAT: String =
            "https://www.osm.org/?mlat=$PATTERN_LOCATION_LATITUDE&mlon=$PATTERN_LOCATION_LONGITUDE&zoom=15"
        const val DEFAULT_DATE_FORMAT = "dd MMMM, yyyy HH:mm"
    }
}
