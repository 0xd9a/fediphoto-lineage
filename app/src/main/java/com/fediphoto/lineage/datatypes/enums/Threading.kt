package com.fediphoto.lineage.datatypes.enums

import androidx.annotation.StringRes
import com.fediphoto.lineage.R

enum class Threading(@StringRes val stringId: Int) {
    NEVER(R.string.threading_never),
    DAILY(R.string.threading_daily),
    ALWAYS(R.string.threading_always)
}
