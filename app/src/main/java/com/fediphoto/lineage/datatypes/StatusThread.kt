package com.fediphoto.lineage.datatypes

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "threads", primaryKeys = ["account_id", "template_id"])
data class StatusThread(
    @ColumnInfo(name = "account_id") val accountId: Int,
    @ColumnInfo(name = "template_id") val templateId: Int,
    @ColumnInfo(name = "status_id") var statusId: String,
    @ColumnInfo(name = "status_date") var statusDate: String
)
