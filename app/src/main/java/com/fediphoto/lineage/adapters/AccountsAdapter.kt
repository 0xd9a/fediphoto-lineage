package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.DialogRemoveAccountBinding
import com.fediphoto.lineage.databinding.ListItemAccountsBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.getRemoveConfirmationDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AccountsAdapter(
    val accounts: MutableList<FediAccount>,
    private val fplData: FPLData,
    private val prefs: Prefs,
    private val lifecycleScope: LifecycleCoroutineScope
) :
    RecyclerView.Adapter<AccountsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ListItemAccountsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val account = accounts[position]

        holder.binding.delete.setOnClickListener { onDeletePressed(holder.binding.root.context, account) }
        holder.binding.account.setAccount(account)
        holder.binding.account.setOnClickListener { setActiveAccount(position, holder.binding, account.id) }
        holder.binding.activeIndicator.setOnClickListener { setActiveAccount(position, holder.binding, account.id) }
        holder.binding.activeIndicator.setImageResource(
            if (account.id == prefs.activeAccountId)
                R.drawable.ic_radio_button_checked
            else
                R.drawable.ic_radio_button_unchecked
        )
    }

    override fun getItemCount(): Int = accounts.size

    inner class ViewHolder(val binding: ListItemAccountsBinding) : RecyclerView.ViewHolder(binding.root)

    private fun setActiveAccount(position: Int, view: ListItemAccountsBinding, accountId: Int) {
        if (prefs.activeAccountId != accountId) {
            var previousActiveItemPosition = position
            for (i in accounts.indices)
                if (accounts[i].id == prefs.activeAccountId)
                    previousActiveItemPosition = i
            prefs.activeAccountId = accountId
            notifyItemChanged(previousActiveItemPosition)
            view.activeIndicator.setImageResource(R.drawable.ic_radio_button_checked)
        }
    }

    private fun onDeletePressed(context: Context, account: FediAccount) {
        val dialogBinding = DialogRemoveAccountBinding.inflate(LayoutInflater.from(context))
        dialogBinding.account.setAccount(account)
        getRemoveConfirmationDialog(context, dialogBinding.root) { removeAccount(account) }.show()
    }

    private fun removeAccount(account: FediAccount) {
        lifecycleScope.launch(Dispatchers.IO) {
            fplData.getAccountDAO().delete(account)
            withContext(Dispatchers.Main) {
                for (i in accounts.indices) {
                    if (accounts[i].id == account.id) {
                        accounts.removeAt(i)
                        notifyItemRemoved(i)
                        if (accounts.isEmpty()) {
                            prefs.activeAccountId = 0
                        } else if (prefs.activeAccountId == account.id) {
                            val index = (accounts.size - 1).coerceAtMost(i)
                            prefs.activeAccountId = accounts[index].id
                            notifyItemChanged(index)
                        }
                        break
                    }
                }
            }
            fplData.getThreadDAO().deleteByAccountId(account.id)
        }
    }
}
