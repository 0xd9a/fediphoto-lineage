package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.database.QueueDAO
import com.fediphoto.lineage.databinding.DialogRemoveQueueItemBinding
import com.fediphoto.lineage.databinding.ListItemQueueBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.getRemoveConfirmationDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class QueueAdapter(
    private val queue: MutableList<QueueItem>,
    private val accounts: Map<Int, FediAccount?>,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val queueDAO: QueueDAO
) : RecyclerView.Adapter<QueueAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ListItemQueueBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val queueItem = queue[position]
        val account = accounts[queueItem.accountId]

        holder.binding.delete.setOnClickListener { onDeletePressed(holder.itemView.context, queueItem, account) }
        if (account != null) {
            holder.binding.account.setAccount(account)
            holder.binding.account.isVisible = true
        } else holder.binding.account.isVisible = false
        holder.binding.template.setHideName(true)
        holder.binding.template.setQueueItem(queueItem)
        holder.binding.template.setPhoto(File(queueItem.photoPath))
        if (queueItem.error != null) {
            holder.binding.error.isVisible = true
            holder.binding.errorText.text = queueItem.error
        } else holder.binding.error.isVisible = false
    }

    override fun getItemCount(): Int = queue.size

    inner class ViewHolder(val binding: ListItemQueueBinding) : RecyclerView.ViewHolder(binding.root)

    private fun onDeletePressed(context: Context, queueItem: QueueItem, account: FediAccount?) {
        val dialogBinding = DialogRemoveQueueItemBinding.inflate(LayoutInflater.from(context))
        if (account != null)
            dialogBinding.account.setAccount(account)
        else
            dialogBinding.account.isVisible = false
        dialogBinding.template.setQueueItem(queueItem)
        getRemoveConfirmationDialog(context, dialogBinding.root) { removeQueueItem(queueItem) }.show()
    }

    private fun removeQueueItem(queueItem: QueueItem) {
        lifecycleScope.launch(Dispatchers.Main) {
            queueItem.modifiedImagePath?.let { File(it) }?.delete()
            val result = queueDAO.delete(queueItem)
            if (result > 0) {
                for (i in queue.indices) {
                    if (queue[i].id == queueItem.id) {
                        queue.removeAt(i)
                        notifyItemRemoved(i)
                        break
                    }
                }
            }
        }
    }
}
