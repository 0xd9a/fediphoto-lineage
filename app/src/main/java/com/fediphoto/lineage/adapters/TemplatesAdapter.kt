package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.Prefs
import com.fediphoto.lineage.R
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.databinding.DialogRemoveTemplateBinding
import com.fediphoto.lineage.databinding.ListItemTemplatesBinding
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.eiffelTowerLocation
import com.fediphoto.lineage.getRemoveConfirmationDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Date

class TemplatesAdapter(
    private val templates: MutableList<Template>,
    private val fplData: FPLData,
    private val prefs: Prefs,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val onEditPressed: (templateId: Int) -> Unit,
    private val scrollListTo: (position: Int) -> Unit
) : RecyclerView.Adapter<TemplatesAdapter.ViewHolder>() {
    private val date = Date()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ListItemTemplatesBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val template = templates[position]
        holder.binding.edit.setOnClickListener { onEditPressed(template.id) }
        holder.binding.delete.setOnClickListener { onDeletePressed(holder.binding.root.context, template) }
        holder.binding.copy.setOnClickListener { copyTemplate(template) }
        holder.binding.template.setTemplate(template, date, eiffelTowerLocation)
        holder.binding.activeIndicator.setOnClickListener { setActiveTemplate(position, holder.binding, template.id) }
        holder.binding.template.setOnClickListener { setActiveTemplate(position, holder.binding, template.id) }
        holder.binding.activeIndicator.setImageResource(
            if (template.id == prefs.activeTemplateId)
                R.drawable.ic_radio_button_checked
            else
                R.drawable.ic_radio_button_unchecked
        )
    }

    override fun getItemCount(): Int = templates.size

    inner class ViewHolder(val binding: ListItemTemplatesBinding) : RecyclerView.ViewHolder(binding.root)

    private fun setActiveTemplate(position: Int, view: ListItemTemplatesBinding, templateId: Int) {
        val prefs = Prefs(view.root.context)
        if (prefs.activeTemplateId != templateId) {
            var previousActiveItemPosition = position
            for (i in templates.indices)
                if (templates[i].id == prefs.activeTemplateId)
                    previousActiveItemPosition = i
            prefs.activeTemplateId = templateId
            notifyItemChanged(previousActiveItemPosition)
            view.activeIndicator.setImageResource(R.drawable.ic_radio_button_checked)
        }
    }

    private fun onDeletePressed(context: Context, template: Template) {
        val dialogBinding = DialogRemoveTemplateBinding.inflate(LayoutInflater.from(context))
        dialogBinding.template.setTemplate(template, date, eiffelTowerLocation)
        getRemoveConfirmationDialog(context, dialogBinding.root) { removeTemplate(template) }.show()
    }

    private fun removeTemplate(template: Template) {
        lifecycleScope.launch(Dispatchers.IO) {
            fplData.getTemplateDAO().delete(template)
            withContext(Dispatchers.Main) {
                for (i in templates.indices) {
                    if (templates[i].id == template.id) {
                        templates.removeAt(i)
                        notifyItemRemoved(i)
                        if (templates.isEmpty()) {
                            prefs.activeTemplateId = 0
                        } else if (prefs.activeTemplateId == template.id) {
                            val index = (templates.size - 1).coerceAtMost(i)
                            prefs.activeTemplateId = templates[index].id
                            notifyItemChanged(index)
                        }
                        break
                    }
                }
            }
            fplData.getThreadDAO().deleteByTemplateId(template.id)
        }
    }

    private fun copyTemplate(template: Template) {
        lifecycleScope.launch(Dispatchers.IO) {
            val rowId = fplData.getTemplateDAO().insert(
                template.apply {
                    id = 0
                    name = "$name (copy)"
                }
            )
            fplData.getTemplateDAO().getByRowId(rowId).let { newTemplate ->
                templates.add(newTemplate)
                withContext(Dispatchers.Main) {
                    notifyItemInserted(templates.lastIndex)
                    scrollListTo(templates.lastIndex)
                }
            }
        }
    }
}
