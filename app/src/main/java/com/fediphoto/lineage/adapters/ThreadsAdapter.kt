package com.fediphoto.lineage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import com.fediphoto.lineage.database.ThreadDAO
import com.fediphoto.lineage.databinding.DialogRemoveThreadBinding
import com.fediphoto.lineage.databinding.DialogTemplatePreviewBinding
import com.fediphoto.lineage.databinding.ListItemThreadsBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.StatusThread
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.eiffelTowerLocation
import com.fediphoto.lineage.getRemoveConfirmationDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Date

class ThreadsAdapter(
    private val threads: MutableList<StatusThread>,
    private val accounts: MutableList<FediAccount>,
    private val templates: MutableList<Template>,
    private val activeAccount: FediAccount?,
    private val activeTemplate: Template?,
    private val date: Date,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val threadDAO: ThreadDAO
) : RecyclerView.Adapter<ThreadsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ListItemThreadsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val thread = threads[position]
        val account = accounts[position]
        val template = templates[position]
        val context = holder.itemView.context

        holder.binding.activeIndicator.isInvisible = !(thread.accountId == activeAccount?.id && thread.templateId == activeTemplate?.id)
        holder.binding.account.setAccount(account)
        holder.binding.templateName.text = template.name
        holder.binding.templateName.setOnClickListener { showTemplatePreview(context, template) }
        holder.binding.lastPostedOn.text = formatThreadDate(thread.statusDate)

        holder.binding.delete.setOnClickListener { threadRemoveConfirmation(context, thread, account, template) }
    }

    override fun getItemCount(): Int = threads.size

    inner class ViewHolder(val binding: ListItemThreadsBinding) : RecyclerView.ViewHolder(binding.root)

    private fun showTemplatePreview(context: Context, template: Template) {
        val dialog = MaterialAlertDialogBuilder(context).create()
        val templatePreviewDialogBinding = DialogTemplatePreviewBinding.inflate(LayoutInflater.from(dialog.context))
        templatePreviewDialogBinding.templateView.setTemplate(template, date, eiffelTowerLocation)
        templatePreviewDialogBinding.button.setOnClickListener { dialog.dismiss() }
        dialog.setView(templatePreviewDialogBinding.root)
        dialog.show()
    }

    private fun threadRemoveConfirmation(context: Context, thread: StatusThread, account: FediAccount, template: Template) {
        val dialogViewBinding = DialogRemoveThreadBinding.inflate(LayoutInflater.from(context))
        val dialog = getRemoveConfirmationDialog(context, dialogViewBinding.root) {}

        dialogViewBinding.account.setAccount(account)
        dialogViewBinding.templateName.text = template.name
        dialogViewBinding.templateName.setOnClickListener { showTemplatePreview(context, template) }
        dialogViewBinding.lastPostedOn.text = formatThreadDate(thread.statusDate)

        dialogViewBinding.buttonCancel.setOnClickListener { dialog.dismiss() }
        dialogViewBinding.buttonRemove.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                val result = threadDAO.delete(thread)
                if (result > 0) {
                    for (i in threads.indices) {
                        val threadsElement = threads[i]
                        if (threadsElement.accountId == account.id && threadsElement.templateId == template.id) {
                            threads.removeAt(i)
                            accounts.removeAt(i)
                            templates.removeAt(i)
                            notifyItemRemoved(i)
                            break
                        }
                    }
                }
            }

            dialog.dismiss()
        }

        dialog.show()
    }

    private fun formatThreadDate(statusDate: String) = StringBuilder(statusDate).insert(6, "-").insert(4, "-")
}
