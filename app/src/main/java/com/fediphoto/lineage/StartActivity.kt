package com.fediphoto.lineage

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.lifecycleScope
import androidx.work.WorkManager
import com.fediphoto.lineage.database.FPLData
import com.fediphoto.lineage.datatypes.enums.AfterPostAction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class StartActivity : AppCompatActivity() {
    private lateinit var prefs: Prefs
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = Prefs(this)
        prefs.canAutoOpenCamera = true
        lifecycleScope.launch {
            if (!prefs.v7MigrationDone) withContext(Dispatchers.IO) { v7Migration() }
            withContext(Dispatchers.Main) {
                AppCompatDelegate.setDefaultNightMode(getThemeMode(this@StartActivity))
                startNextActivity()
            }
        }
    }

    private fun startNextActivity() {
        startActivity(Intent(this, if (prefs.introOnStart) IntroActivity::class.java else MainActivity::class.java))
        finish()
    }

    private suspend fun v7Migration() {
        val oldDBFile = getDatabasePath(Database.DB_NAME)
        if (oldDBFile.exists()) {
            WorkManager.getInstance(this).cancelAllWork()

            val database = Database(this)

            val fplData = FPLData.getInstance(this)
            val accountDAO = fplData.getAccountDAO()
            val templateDAO = fplData.getTemplateDAO()
            val queueDAO = fplData.getQueueDAO()
            val threadDAO = fplData.getThreadDAO()

            val oldPrefs = OldPrefs(this)

            database.getAccounts().forEach {
                val rowId = accountDAO.insert(it.copy(id = 0))
                if (it.id == oldPrefs.activeAccountId) {
                    val account = accountDAO.getByRowId(rowId)
                    prefs.activeAccountId = account.id
                }
            }
            database.getTemplates().forEach {
                val rowId = templateDAO.insert(it.copy(id = 0))
                if (it.id == oldPrefs.activeTemplateId) {
                    val template = templateDAO.getByRowId(rowId)
                    prefs.activeTemplateId = template.id
                }
            }
            database.getQueueList().forEach { queueDAO.insert(it.copy(id = 0)) }
            database.getThreads().forEach { threadDAO.insert(it) }

            prefs.theme = oldPrefs.theme.name.lowercase()
            prefs.language = oldPrefs.language
            prefs.introOnStart = !oldPrefs.introDisplayed
            prefs.cameraOnStart = oldPrefs.cameraOnStart
            prefs.previewBeforePost = oldPrefs.previewBeforePost
            prefs.savePhoto = oldPrefs.afterPostAction == AfterPostAction.COPY || oldPrefs.afterPostAction == AfterPostAction.MOVE
            prefs.locationService = oldPrefs.locationService

            database.close()
            oldDBFile.delete()
        }

        prefs.v7MigrationDone = true

        startWorkerPost(this)
    }
}
