package com.fediphoto.lineage.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import coil.load
import com.fediphoto.lineage.R
import com.fediphoto.lineage.databinding.CustomViewAccountBinding
import com.fediphoto.lineage.datatypes.FediAccount
import com.fediphoto.lineage.datatypes.OSMAccount

class AccountView : FrameLayout {
    private var binding = CustomViewAccountBinding.inflate(LayoutInflater.from(context), this, true)
    private var fediAccount: FediAccount? = null

    private var onClick: ((account: FediAccount) -> Unit)? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    fun setAccount(account: FediAccount) {
        this.fediAccount = account
        updateView(
            account.avatar,
            account.displayName,
            String.format("@%s@%s", account.username, account.instance)
        )
    }

    fun setAccount(account: OSMAccount) {
        this.fediAccount = null
        updateView(
            account.avatar,
            account.displayName,
            account.id
        )
    }

    fun setOnClickListener(onClick: (account: FediAccount) -> Unit) {
        this.onClick = onClick
    }

    private fun updateView(avatar: String?, displayName: String, username: String) {
        binding.avatar.load(avatar ?: R.drawable.ic_account) { placeholder(R.drawable.ic_account) }
        binding.displayName.text = displayName.ifBlank { username }
        binding.username.text = username
        fediAccount.let { account ->
            if (account != null)
                binding.root.setOnClickListener { this.onClick?.invoke(account) }
            else
                binding.root.setOnClickListener(null)
        }
    }
}
