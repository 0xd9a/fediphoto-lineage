package com.fediphoto.lineage.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import coil.load
import com.fediphoto.lineage.createContent
import com.fediphoto.lineage.databinding.CustomViewTemplateBinding
import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.QueueItem
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility
import com.fediphoto.lineage.datatypes.toTemplate
import java.io.File
import java.util.Date

class TemplateView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private val binding = CustomViewTemplateBinding.inflate(LayoutInflater.from(context), this, true)
    private var hideName = false

    fun setTemplate(template: Template, date: Date?, location: Location?) = updateView(template, date, location)

    fun setQueueItem(queueItem: QueueItem) {
        hideName = true
        updateView(queueItem.toTemplate(), queueItem.dateValue, queueItem.locationValue)
    }

    private fun updateView(template: Template, date: Date?, location: Location?) {
        updateView(
            template.spoilerText.takeIf { template.spoilerEnabled },
            createContent(template, date, location),
            template.sensitiveMedia,
            template.visibility,
            template.threading,
            template.name
        )
    }

    private fun updateView(
        spoilerText: String?,
        content: String?,
        sensitiveMedia: Boolean,
        visibility: Visibility,
        threading: Threading,
        name: String?
    ) {
        if (!spoilerText.isNullOrBlank()) {
            binding.cvtSpoilerText.isVisible = true
            binding.cvtSpoilerText.text = spoilerText
        } else binding.cvtSpoilerText.isVisible = false
        binding.cvtContentText.text = content
        binding.cvtPhoto.isVisible = false

        if (!hideName) {
            binding.cvtName.isVisible = true
            binding.cvtName.text = name
        } else binding.cvtName.isVisible = false

        binding.cvtSensitiveMedia.isVisible = sensitiveMedia

        binding.cvtVisibility.setText(visibility.stringId)
        val compoundDrawable = ResourcesCompat.getDrawable(resources, visibility.drawableId, context.theme)
        when (layoutDirection) {
            View.LAYOUT_DIRECTION_LTR -> binding.cvtVisibility.setCompoundDrawablesWithIntrinsicBounds(compoundDrawable, null, null, null)
            View.LAYOUT_DIRECTION_RTL -> binding.cvtVisibility.setCompoundDrawablesWithIntrinsicBounds(null, null, compoundDrawable, null)
        }

        binding.cvtThreading.setText(threading.stringId)
    }

    fun setPhoto(file: File) {
        binding.cvtPhoto.load(file)
        binding.cvtPhoto.isVisible = true
    }

    fun setHideName(hide: Boolean) {
        hideName = hide
    }
}

