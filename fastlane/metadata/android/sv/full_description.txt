Android-app för att snabbt lägga upp foton till Fediverse.

PLA skrev den här appen för att klia en klåda som var att posta foton till Fediverse friktionsfritt när han cyklade.
Vi fortsätter hans projekt för att hedra hans minne.

Du konfigurerar texten och hashtags för status i förväg. Lägg sedan upp ett foto med 3 tryck:
* Tryck för kamera.
* Rama in motivet och tryck för att ta foto.
* Tryck för att acceptera foto och du är klar!

Postningen av fotot och statustexten görs i bakgrunden.
Om anslutningen är dålig laddas den upp när anslutningen tillåter.

Funktioner:
- Flera federala konton kan läggas till
- Flera statuskonfigurationer kan ställas in i förväg
- Förhandsgranska före inlägg eller inlägg direkt efter presstagning
- Inbyggd platstjänst (ifall kameran inte kan ge plats)
- Offlineläge (ladda upp inlägg/foton när internet är tillbaka)
- Hantera tagna foton (behåll eller radera)
- Skapa OSM-anteckningar med dina foton
- Importera/exportera inställningar och konton

https://silkevicious.codeberg.page/fediphoto-lineage.html
